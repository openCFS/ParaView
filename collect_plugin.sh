#!/bin/sh

# this script collects all the plugin files from an archive created by the metabuild via `ctest -P cpack-paraview`
# the archive must be supplied as first argument e.g.
# ./collect_plungin.sh ParaView-5.2.0-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz
# will generate CFSReader_Linux_ParaView-v5.2.0.tar.gz
# for installation instructions see [1](http://cfs-doc.mdmt.tuwien.ac.at/mediawiki/index.php/Installation)

CDIR=$(pwd) # current dir

# supply packaged paraview as argument
case "$1" in
 /*) # absolute path
  archive=$1 ;;
 *) # relative path
  archive=$CDIR/$1 ;;
esac

# extract version string
version=`echo $archive | sed 's/.\+ParaView-\([^-]\+\)-.\+/\1/'`
echo 'Version '$version
# define plugin name
plugin=CFSReader_Linux_ParaView-v$version

# extract to tmp
cd /tmp
rm -rf $plugin
mkdir $plugin && cd $plugin
echo
echo 'extrancting files from '$archive
tar -xzvf $archive --wildcards '*/lib/libhdf5_cpp*' --strip-components=1
tar -xzvf $archive --wildcards '*/lib/libboost_filesystem*' --strip-components=1
tar -xzvf $archive --wildcards '*/lib/libboost_system*' --strip-components=1
tar -xzvf $archive --wildcards '*/lib/libvtknetcdf*' --strip-components=1
tar -xzvf $archive --wildcards '*/lib/libvtkNetCDF*' --strip-components=1
tar -xzvf $archive  --wildcards '*/lib/paraview-*/plugins/CFSReader/*' --strip-components=5
mkdir lib/plugins # create autoload location
mv libCFSReader.so lib/plugins # move there
# pack together
echo
echo 'packing to '$CDIR'/'$plugin'.tar.gz'
tar -czvf $CDIR/$plugin'.tar.gz' *
