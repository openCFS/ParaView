ParaView for openCFS
====================

This project is intended to provide the ParaView plugin for reading the native HDF5 data format of openCFS.
Since ParaView's build process changed dramatically with v5.7/v5.8 most of this project (and the documentation in the following is outdated).
The source of the current plugin is found in [plugins/CFSReader](plugins/CFSReader), and should be included in upstream ParaView in the future.

OVERVIEW
========
Paraview is an open-source visualizer. By a CFSReader-plugin hdf5-files with the CFS/NACS data structure can be read.
To build the plugin it is also necessary to build paraview.
Paraview is built via the [paraview-superbuild](https://gitlab.kitware.com/paraview/paraview-superbuild/).

The paraview-superbuild allows to build almost all dependencies instead of using system installed code.
This makes it easy to create a distributable Paraview with integrated CFSReader plugin.
However, one can also use the plugin along with an binary download of Paraview.
For this it is necessary to build in the official build system, see [`centos6/README.md`](centos6/README.md).
The process is anyway the same, we build Paraview and the plugin and either distribute all (`ctest -R cpack`) or extract the plugin and the necessary libs.

The plugin requires the libs `boost_filesystem` and `hdf5_cpp` not built normally by paraview-superbuild.
Therefore, we patch the superbuild to also build these libs.
We call patching superbuild and integrating the plugin a "metabuild".
You can do all automatically via the metabuild or manually.
The later might be more convenient for plugin development.
The resulting plugin is the same.

METABUILD
=========
Create a build directory, e.g. `<metabuild>` **outside** of this repo, go there and do
`cmake <repo-dir>` and `make` afterwards.

CMake options are:
* `-Ddownload_location:PATH=...`, where the default is /code/cfsdepscache/paraview
* `-DSKIP_PLUGIN:BOOL=ON` for only patching and building the paraview-superbuild
* `-DBUNDLE_PYTHON:BOOL=OFF` to switch off building local Python and mpi
* `-DRENDERING_ENGINE:STRING=qt5` or, `qt4`, `egl`, `mesa`, `osmesa`. Note that qt5 is default for PV >= 5.3

The sources will be in `metabuild/src/paraview`.
It will be built in `metabuild/build` with the results in `metabuid/install`.
You can create distributables by packing the install directory or running `ctest -R cpack`
in `metabuild`.

You can always build the Plugin manually by creating in `plugins/CFSReader` a build directory and doing `cmake ..` there.

MANUAL BUILD
============
This procedure is only meant to help to understand what the metabuild does.

We assume a directory `<pvsb>` as base. For the metabuid this would be e.g. `~/code/cfs_paraview/metabuild`.

Fetch paraview-superbuild
-------------------------
To be compatible with the metabuild (not necessary) we create `<pvsb>/src`.
The metabuild builds paraview-superbuild as external project and this requires the `src` directory.

Following https://gitlab.kitware.com/paraview/paraview-superbuild/
```
cd <pbsb>/src
git clone --recursive https://gitlab.kitware.com/paraview/paraview-superbuild.git
cd paraview-superbuild
git fetch origin
git checkout master # or a tag, see CMakeLists.txt
git submodule update
```

Building paraview-superbuild
----------------------------
At this point we need to patch the superbuild in `<pbsb>/src/paraview-superbuild`.
See the `pvsb-*.patch` files.

Create a build directory, e.g. `<metabuild>/build` and run there
```
cmake ../paraview-superbuild -DCMAKE_BUILD_TYPE:STRING=Release -DBUILD_TESTING=ON -DENABLE_cxx11=ON -DENABLE_qt5=ON -DENABLE_boost:BOOL=ON -DENABLE_hdf5:BOOL=ON -DENABLE_zlib:BOOL=ON -DENABLE_png:BOOL=ON -Dsuperbuild_download_location=$HOME/code/cfsdepscache/paraview
```
you might want to add `-DENABLE_python:BOOL=ON -DUSE_SYSTEM_python=ON`.
For OSX add `-DCMAKE_OSX_SDK=macosx10.12`.

Building the Plugin
-------------------
Either add it to the paraview-superbuild (second configure is ok)
```
-DENABLE_paraviewpluginsexternal:BOOL=ON -Dparaview_PLUGINS_EXTERNAL:STRING=CFSReader -Dparaview_PLUGIN_CFSReader_PATH:PATH=<CFS++_Paraview>/plugins/CFSReader
```
Or build the plugin manually in a own build directory, e.g. `<CFS++_Paraview>/plugins/CFSReader/build`.
If you follow the suggestions for the directories, a `cmake ..` is sufficient there, otherwise give `-DPVSB_BUILD` to `<metabuild>`.

Automatic Build via Gitlab
==========================

We have a gitlab runner configured to build the PV and the plugin according to the official build system.
The plugin can then be combined with the official PV builds.
You can download the packaged binaries of ParaView and the Plugin as 
[**artifacts**](https://cfs-dev.mdmt.tuwien.ac.at/cfs/ParaView/-/jobs/artifacts/master/download?job=build) 
from our gitlab server.
Artifacts expire after one month, so you may have to [run a new pipeline](https://cfs-dev.mdmt.tuwien.ac.at/cfs/ParaView/pipelines/new).
If something went wrong with a previous job, you can trigger a complete, clean rebuild by running the pipeline via a [pipeline schedule](https://cfs-dev.mdmt.tuwien.ac.at/cfs/ParaView/pipeline_schedules) (just klick play for the branch you want).
See [`centos6/README.md`](centos6/README.md) for details.

TIPPS
=====
One can check the configurations of the projects (paraview, hdf5) in `<metabuild>/build` via `ccmake`.

Updating to a new ParaView release
----------------------------------

It is usually as simple as changing the CMake cache variable `GITTAG` to the desired version.
However, one needs to keep in mind the following things
* the superbuild will not update the paraview source if it is already present in the build directory -> remove it or start from an empty build directory.
* pipeline builds on the same branch keep the build dir -> create a new branch for a new build or create a schedule (which will run the clear stage).

Once the pipeline has succeded, test the plugin
1. download and install the assoiciated [official ParaView build](https://www.paraview.org/download/)
2. download the plugin (=build artifact), and install it `tar -xzvf CFSReader_Linux_ParaView-v5.0.1.tar.gz -C <paraview-dir>`
3. test to load a CFS result

Finally, create a **tag** in the git repository.

ERROR HANDLING
--------------

No package 'freetype2' found --> install libfreetype2-dev / libfreetype2-devel or and try `export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig`
No package 'libxml2' found --> install libxml2-dev / libxml2-devel

If the build of qt5 fails, check the log in `./build/superbuild/qt5/build/config.log`.
Look for tests which are marked as `FAILED` due to missing header files (e.g. `libudev.h`) and install corresponding development packages.
