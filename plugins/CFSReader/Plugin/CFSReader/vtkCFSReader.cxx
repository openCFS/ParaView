
#include "vtkCFSReader.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkInformationStringKey.h"
#include "vtkObjectFactory.h"
#include "vtkErrorCode.h"
#include "vtkUnstructuredGrid.h"
#include "vtkPointData.h"
#include "vtkCellData.h"
#include "vtkDoubleArray.h"
#include "vtkUnsignedIntArray.h"
#include "vtkIntArray.h"
#include "vtkCellArray.h"

#include "vtkStreamingDemandDrivenPipeline.h"


#include <vtkVertex.h>
#include <vtkLine.h>
#include <vtkQuadraticEdge.h>
#include <vtkTriangle.h>
#include <vtkQuadraticTriangle.h>
#include <vtkQuad.h>
#include <vtkQuadraticQuad.h>
#include <vtkBiQuadraticQuad.h>
#include <vtkTetra.h>
#include <vtkQuadraticTetra.h>
#include <vtkHexahedron.h>
#include <vtkQuadraticHexahedron.h>
#include <vtkTriQuadraticHexahedron.h>
#include <vtkPyramid.h>
#include <vtkQuadraticPyramid.h>
#include <vtkWedge.h>
#include <vtkQuadraticWedge.h>


#include <sstream>
#include <cmath>
#include <list>
#include <boost/format.hpp>
#include "boost/lexical_cast.hpp"

#ifdef CFS_GUI
// include Python in order to release the global interpreter
// lock to allow for concurrent reading in the mesh 
#include "python2.7/Python.h"
#endif

// **************************************************
//  For debugging, please define macro in next line
// **************************************************
#define DEBUG 0  /* set to 1 for debugging */


#if DEBUG >  0 
#if _WIN32
// Note: Under windows we normally do not get access to the 
// standard streams. Thus we use the vtk / paraview output window.
#define DBG_OUT(arg) {                                                        \
vtkOStreamWrapper::EndlType endl;                                             \
    vtkOStreamWrapper::UseEndl(endl);                                         \
    vtkOStrStreamWrapper vtkmsg;                                              \
    vtkmsg << "Debug: In " __FILE__ ", line " << __LINE__ << "\n"             \
           << this->GetClassName() << " (" << this << "): " arg  << "\n\n";   \
    vtkOutputWindowDisplayDebugText(vtkmsg.str());                            \
	vtkmsg.rdbuf()->freeze(0);   }
#else
#define DBG_OUT(arg) std::cerr << "line " << __LINE__ << ": " << arg << std::endl;
#endif
#else
#define DBG_OUT(arg)
#endif

vtkStandardNewMacro(vtkCFSReader);

const double pi = std::acos( -1.0 );


// Initialize Information Keys 
vtkInformationKeyMacro(vtkCFSReader, RESULT_NAME, String);
vtkInformationKeyMacro(vtkCFSReader, DOF_NAMES, StringVector);
vtkInformationKeyMacro(vtkCFSReader, DEFINED_ON, Integer);
vtkInformationKeyMacro(vtkCFSReader, ENTITY_NAME, String);
vtkInformationKeyMacro(vtkCFSReader, ENTRY_TYPE, Integer);
vtkInformationKeyMacro(vtkCFSReader, STEP_NUMS, IntegerVector);
vtkInformationKeyMacro(vtkCFSReader, STEP_VALUES, DoubleVector);
vtkInformationKeyMacro(vtkCFSReader, UNIT, String);
vtkInformationKeyMacro(vtkCFSReader, ENTITY_IDS, StringVector);
vtkInformationKeyMacro(vtkCFSReader, MULTI_SEQ_INDEX, Integer);
vtkInformationKeyMacro(vtkCFSReader, ANALYSIS_TYPE, Integer);


// taken from vtkSetStringMacro and enhanced to reset history result info
void vtkCFSReader::SetFileName(const char* _arg) {
  if ( this->FileName == NULL && _arg == NULL) return;
  if ( this->FileName && _arg && (!strcmp(this->FileName,_arg))) return;
  
  in_.CloseFile();
  delete [] this->FileName;
  
  if (_arg) {
    size_t n = strlen(_arg) + 1;
    char *cp1 =  new char[n];
    const char *cp2 = (_arg);
    this->FileName = cp1;
    do {
      *cp1++ = *cp2++;
    } while ( --n );
  } else {
    this->FileName = NULL;
  }
  this->Modified();
  hdf5InfoRead = false; // re-initialize info on history results
}

void vtkCFSReader::CloseFile() {
  in_.CloseFile();
  this->FileName = NULL;
}

void vtkCFSReader::SetMultiSequenceStep( int step  ) {
  DBG_OUT("Setting multisequence step " << step << " for a valid range of "
           << this->MultiSequenceRange[0]  << ", " << this->MultiSequenceRange[1] );
  
  //this->ReadHdf5Info();

  if( step == this->MultiSequenceStep )
    return;
  if( step > this->MultiSequenceRange[1] || 
      step < this->MultiSequenceRange[0] ) {
    DBG_OUT("Please enter a valid multisequence step between "
        << this->MultiSequenceRange[0] << " and " 
        << this->MultiSequenceRange[1] << "!");
    vtkErrorMacro(<< "Please enter a valid multisequence step between "
                  << this->MultiSequenceRange[0] << " and " 
                  << this->MultiSequenceRange[1] << "!" << "selected value was " << step << "\n");
  }
 
  this->MultiSequenceStep = step;
  this->msStepChanged = true;
  
  // In addition trigger resetting the data value arrays, as the results in 
  // the new msStep can be different than from the previous one.
  this->resetDataArrays = true;
  
  // Important: Set state to "modified", so that the result information
  // gets updated
  this->Modified();
  DBG_OUT("Step has changed");
}

void vtkCFSReader::SetTimeStep (unsigned int step) {
  vtkDebugMacro(<< this->GetClassName() << " (" << this << "): setting TimeStep to " << step);
  DBG_OUT("=> Set Time Step to " << step);
  DBG_OUT("\tPrevious step was " << this->TimeStep)
  DBG_OUT("\tSize of stepVals is" << this->stepVals.size())
  
  // security check: immediately leave, if no time / frequency steps are available
  if( this->stepVals.size() == 0 )
    return;
  step = step+1;

  if (this->TimeStep != step && this->stepVals.size() >= step ) 
  { 
    this->TimeStep = step; 
    this->timeFreqVal = stepVals[step-1];
    DBG_OUT("\tTime / Freq VAL is" << this->timeFreqVal);
    
    if( harmonicData )
      PrettyPrintFreq(this->timeFreqVal, this->timeFreqValStr );
    else 
      PrettyPrintTime(this->timeFreqVal, this->timeFreqValStr );
  
    
    // update pipeline
    this->Modified();
  }
}

const char* vtkCFSReader::GetTimeFreqValStr() {
  if( !this->harmonicData || !this->HarmonicDataAsModeShape) {
    return "-";
  } else {
    return timeFreqValStr.c_str();
  }
}

void vtkCFSReader::SetAddDimensionsToArrayNames(int flag) {
  this->AddDimensionsToArrayNames = flag;

  // In addition trigger resetting the data value arrays 
  this->resetDataArrays = true;
  
  // update pipeline
  this->Modified();
}

void vtkCFSReader::SetHarmonicDataAsModeShape(int flag) {
  this->HarmonicDataAsModeShape = flag;
  
  if (flag == 1 && harmonicData) {
    this->TimeStepNumRange[1] = this->NumberOfTimeSteps-1;
  } else {
    this->TimeStepNumRange[1] = 0;
  }
  
  // In addition trigger resetting the data value arrays 
  this->resetDataArrays = true;
  
  // update pipeline
  this->Modified();
}


void vtkCFSReader::SetFillMissingResults(int flag) {
  this->FillMissingResults = flag;
  
  // In addition trigger resetting the data value arrays 
  this->resetDataArrays = true;  
  
  // update pipeline
  this->Modified();
}

int vtkCFSReader::GetNumberOfRegionArrays() {
  return regionNames_.size();
}

int vtkCFSReader::GetGridDimension(void) {
  return dimension_;
}

int vtkCFSReader::GetGridOrder(void) {
  return gridOrder_;
}

int vtkCFSReader::GetRegionArrayStatus( const char * name ){
  std::map<std::string,int>::const_iterator it = regionSwitch.find(name);
  if( it == regionSwitch.end() ) {
    vtkErrorMacro(<< "Region '" << name << "' not found.");
  }
  return it->second;
  
}

void vtkCFSReader::SetRegionArrayStatus( const char * name, int status ){
  regionSwitch[name] = status;
  
  // IMPORTANT: Let VTK know, that the pipeline needs an update
  // (otherwise nothing would happen)
  this->Modified();
  
  // In addition, set flag to false, that regions have to be updated
  this->activeRegionsChanged = true;
}
const char * vtkCFSReader::GetRegionArrayName( int index) {
  return regionNames_[index].c_str();
}

int vtkCFSReader::GetNumberOfNamedNodeArrays() {
  return namedNodeNames_.size();
}

int vtkCFSReader::GetNamedNodeArrayStatus( const char * name ){
  std::map<std::string,int>::const_iterator it = namedNodeSwitch.find(name);
  if( it == namedNodeSwitch.end() ) {
    vtkErrorMacro(<< "Named nodes '" << name << "' not found.");
  }
  return it->second;
}

void vtkCFSReader::SetNamedNodeArrayStatus( const char * name, int status ){
  namedNodeSwitch[name] = status;
  this->Modified();
  // In addition, set flag to false, that regions have to be updated
  this->activeRegionsChanged = true;
}

const char * vtkCFSReader::GetNamedNodeArrayName( int index) {
  return namedNodeNames_[index].c_str();
}

int vtkCFSReader::GetNumberOfNamedElemArrays() {
  return namedElemNames_.size();
}

int vtkCFSReader::GetNamedElemArrayStatus( const char * name ){
  std::map<std::string,int>::const_iterator it = namedElemSwitch.find(name);
  if( it == namedElemSwitch.end() ) {
    vtkErrorMacro(<< "Named elems '" << name << "' not found.");
  }
  return it->second;
}

void vtkCFSReader::SetNamedElemArrayStatus( const char * name, int status ){
  namedElemSwitch[name] = status;
  this->Modified();
  // In addition, set flag to false, that regions have to be updated
  this->activeRegionsChanged = true;
}

const char * vtkCFSReader::GetNamedElemArrayName( int index) {
  return namedElemNames_[index].c_str();
}

//----------------------------------------------------------------------------
vtkCFSReader::vtkCFSReader()
{
  this->FileName = NULL;
  this->SetNumberOfInputPorts(0);
  this->SetNumberOfOutputPorts(1);
  
  // Define Debugging
  this->SetDebug(0);
  this->mbDataSet = NULL;
  this->mbActiveDataSet = NULL;
  this->dimension_ = 0;
  this->gridOrder_ = 0;

  this->isInitialized = false;
  this->hdf5InfoRead = false;
  this->activeRegionsChanged = true;
  this->resetDataArrays = false;
  this->timeFreqVal = 0.0;
  this->timeFreqValStr = "0.0";
  this->TimeStep = 1;
  this->MultiSequenceStep = 1;
  this->MultiSequenceRange[0] = 1;
  this->MultiSequenceRange[1] = 1;
  this->NumberOfTimeSteps = 0;
  this->TimeStepNumRange[0] = 1;
  this->TimeStepNumRange[1] = 1;
  this->TimeStepValuesRange[0] = 0.0;
  this->TimeStepValuesRange[1] = 1.0;
  this->msStepChanged = true;
  this->HarmonicDataAsModeShape = false;
  this->harmonicData = false;
  this->FillMissingResults = 0;
}

//----------------------------------------------------------------------------
vtkCFSReader::~vtkCFSReader()
{
  if (this->FileName)
    {
    delete [] this->FileName;
    }
}

//----------------------------------------------------------------------------

int vtkCFSReader::CanReadFile(const char* fname) {
  
  // Try to "load" the file (which internally just opens the mesh group)
  // to see, if this is a true CFS HDF5 file. 
  std::string fileName = std::string(fname);
  H5CFS::Hdf5Reader temp; 
  try {
    temp.LoadFile( fileName );
    // If this succeeds, we assume that this is a true CFS HDF5 file.
    return true;
  } catch (...) {
    return false;
  }
}


//----------------------------------------------------------------------------
void vtkCFSReader::ReadHdf5Info() {
 
  // By default enable groups in the CFS GUI, but not in ParaView 
#ifdef CFS_GUI
  const int enableGroups = 1;
#else
  const int enableGroups = 0;
#endif

  // Load hdf file (only done first time)
  if( !hdf5InfoRead ) {
    try {
      in_.LoadFile(this->FileName);
      this->dimension_ = in_.GetDim();
      this->gridOrder_ = in_.GetGridOrder();
      in_.GetAllRegionNames( regionNames_ );
      in_.GetNodeNames( namedNodeNames_ );
      in_.GetElemNames( namedElemNames_ );
      
      for( unsigned int i = 0, n = regionNames_.size(); i < n; i++ ) {
        std::map<std::string,int>::iterator it = regionSwitch.find(regionNames_[i]);
        if( it == regionSwitch.end() ) {
          regionSwitch[regionNames_[i]] = 1;
        }

        // vtkDebugMacro("**** " << regionNames_[i] << " " << regionSwitch[regionNames_[i]] << " ****");
      }

      for( unsigned int i = 0, n = namedNodeNames_.size(); i < n; i++ ) {
        std::map<std::string,int>::iterator it = namedNodeSwitch.find(namedNodeNames_[i]);
        if( it == namedNodeSwitch.end() ) {
          namedNodeSwitch[namedNodeNames_[i]] = enableGroups;
        }

        // vtkDebugMacro("+++++ " << namedNodeNames_[i] << " " << namedNodeSwitch[namedNodeNames_[i]] << " ++++");
      }

      for( unsigned int i = 0, n = namedElemNames_.size(); i < n; i++ ) {
        std::map<std::string,int>::iterator it = namedElemSwitch.find(namedElemNames_[i]);
        if( it == namedElemSwitch.end() ) {
          namedElemSwitch[namedElemNames_[i]] = enableGroups;
        }

        // vtkDebugMacro("---- " << namedElemNames_[i] << " " << namedElemSwitch[namedElemNames_[i]] << " ----");
      }
      
      // store info on multisequence steps with mesh results
      std::map<unsigned int, unsigned int> numSteps;
      in_.GetNumMultiSequenceSteps(resAnalysisType_, numSteps, false);
      
      // Get available mesh results
      std::map<unsigned int, H5CFS::AnalysisType>::const_iterator
          msIt = resAnalysisType_.begin(), msEnd = resAnalysisType_.end();
      for ( ; msIt != msEnd; ++msIt) {
        in_.GetResultTypes(msIt->first, meshResultInfos_[msIt->first], false);
      }
      
      // store info on multisequence steps for history results
      in_.GetNumMultiSequenceSteps(histAnalysisType_, numSteps, true);
      
      // Get available history results
      msIt = histAnalysisType_.begin();
      msEnd = histAnalysisType_.end();
      for ( ; msIt != msEnd; ++msIt) {
        in_.GetResultTypes(msIt->first, histResultInfos_[msIt->first], true);
      }
      
    } catch( std::exception& ex ) {
      vtkErrorMacro(<< "Caught exception when trying to read file '"
                    << this->FileName << "' : '" 
                    << ex.what() << "'");
    } catch(std::string& ex ) {
      vtkErrorMacro(<< "Caught exception when trying to read file '"
                    << this->FileName << "' : '" 
                    << ex  << "'");
    }
    
    hdf5InfoRead = true;
    this->msStepChanged = true;
    isInitialized = false;
  } // info red
  
}


//----------------------------------------------------------------------------
int vtkCFSReader::RequestInformation(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **vtkNotUsed(inputVector),
  vtkInformationVector *outputVector)
{

 DBG_OUT(   "============================\n"
         << " Calling RequestInformation\n"
         << "============================\n")
 
 // Read basic information from HDF5
 ReadHdf5Info();
 
 try {
   
   
   // ----------------------------------
   //  Check for new multisequence step
   // ----------------------------------
   if( this->msStepChanged ) {
     DBG_OUT("===> New MS Step: " << this->MultiSequenceStep );
     // Request number of multiSequenceSteps

     // adjust range for
     if (resAnalysisType_.size() > 0) {
       this->MultiSequenceRange[0] = resAnalysisType_.begin()->first;
       this->MultiSequenceRange[1] = resAnalysisType_.rbegin()->first;

       // check, if any result at all are present
       // Run over all result types
       std::map<unsigned int, double> globSteps;

       if (resAnalysisType_.find(MultiSequenceStep) != resAnalysisType_.end()) {
         // store current analysis type
         this->analysisType = resAnalysisType_[MultiSequenceStep]; 
         if( this->analysisType == H5CFS::HARMONIC ||
	     this->analysisType == H5CFS::EIGENVALUE ||
             this->analysisType == H5CFS::EIGENFREQUENCY) {
           harmonicData = true;
         } else {
           harmonicData = false;
         }
  
         for( unsigned int i = 0; i < meshResultInfos_[MultiSequenceStep].size(); ++i ) {
           std::map<unsigned int, double> actStepVals;
           in_.GetStepValues( MultiSequenceStep,
                              meshResultInfos_[MultiSequenceStep][i],
                              actStepVals, false );
           std::map<unsigned int, double>::const_iterator it;
           for( it = actStepVals.begin(); it != actStepVals.end(); it++ ) {
             globSteps[it->first] = it->second;
             DBG_OUT("Timestep: " << it->first << " = " << it->second);
           }
         }
       }
       
       this->stepNums.resize( globSteps.size() );
       this->stepVals.resize( globSteps.size() );
       std::map<unsigned int, double>::const_iterator itStep = globSteps.begin(),
           endStep = globSteps.end();
       for (unsigned int i = 0; itStep != endStep; ++itStep, ++i) {
         stepNums[i] = itStep->first;
         stepVals[i] = itStep->second;
       }
     }  
     this->NumberOfTimeSteps = this->stepNums.size();
     DBG_OUT( "Simulation has " << NumberOfTimeSteps << " time / frequency steps");
     if( harmonicData && HarmonicDataAsModeShape) { 
       this->TimeStepNumRange[1] = this->NumberOfTimeSteps-1;
     } else {
       this->TimeStepNumRange[1] = 0;
     }
     if(this->NumberOfTimeSteps) 
     {
       this->TimeStepValuesRange[0] = this->stepVals[0];
       this->TimeStepValuesRange[1] = this->stepVals[NumberOfTimeSteps-1];
     }
     DBG_OUT( "Time Step Range  [" << this->TimeStepValuesRange[0]
              << ", "  << this->TimeStepValuesRange[1] );
                                                                
     this->msStepChanged = false;
   } // if multisequence step has changed
   
 } catch(std::string& ex ) {
   vtkErrorMacro(<< "Caught exception when trying to read file '"
                 << this->FileName << "' : '" 
                 << ex  << "'");
 } catch(const char *  ex ) {
   vtkErrorMacro(<< "Caught exception when trying to read file '"
                 << this->FileName << "' : '" 
                 << ex  << "'");
 }

 if( this->NumberOfTimeSteps > 0 ) {
   if(harmonicData && HarmonicDataAsModeShape) {
     // From harmonic data we can produce a continuous time data set which ranges from 0 to one
     // All other kinds of data are discrete in time/freq.

     this->TimeStepValuesRange[0] = 0;
     this->TimeStepValuesRange[1] = 1.0;
     DBG_OUT( "Time Step Range  [" << this->TimeStepValuesRange[0]
                 << ", "  << this->TimeStepValuesRange[1] );

     outputVector->GetInformationObject(0)->Remove(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
   } else {    
     outputVector->GetInformationObject(0)->Set(vtkStreamingDemandDrivenPipeline::TIME_STEPS(), 
                                                &(this->stepVals[0]), this->NumberOfTimeSteps);
   }

   outputVector->GetInformationObject(0)->
       Set(vtkStreamingDemandDrivenPipeline::TIME_RANGE(), TimeStepValuesRange, 2);
 }

 return 1;
}
//----------------------------------------------------------------------------
int vtkCFSReader::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **vtkNotUsed(inputVector),
  vtkInformationVector *outputVector)
{


  DBG_OUT( "============================\n"
           << " Calling RequestData\n"
           << "============================\n" );

  // get the info object
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the ouptut
  vtkMultiBlockDataSet *output = vtkMultiBlockDataSet::SafeDownCast(
     outInfo->Get(vtkMultiBlockDataSet::DATA_OBJECT()));


  // ===================
  // Time Stepping Stuff
  // (taken from OpenFAOMReader)
  // ===================
  DBG_OUT( "Current time step before is " << TimeStep );
  
  // Attention: Only change value of pipeline / step values, if 
  // there are any results present at all.
  if(outInfo->Has(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP())
      && stepVals.size() > 0 )
  {
    requestedTimeValue
    = outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP());
    int nSteps
    = outInfo->Length(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
    double* rsteps
    = outInfo->Get(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
    int timeI = 0;
    while(timeI < nSteps - 1 && rsteps[timeI] < requestedTimeValue)
    {
      timeI++;
    }

    if(harmonicData && HarmonicDataAsModeShape) {
      outInfo->Set(vtkDataObject::DATA_TIME_STEP(), requestedTimeValue);
    } else {
      outInfo->Set(vtkDataObject::DATA_TIME_STEP(), stepVals[timeI]);
      this->TimeStep = timeI+1;
      this->timeFreqVal = stepVals[timeI];
      if( harmonicData )
        PrettyPrintFreq(this->timeFreqVal, this->timeFreqValStr );
      else 
        PrettyPrintTime(this->timeFreqVal, this->timeFreqValStr );

      DBG_OUT( "Current time step is " << TimeStep );
    }
  }

  // Read in data from file
  this->ReadFile(output);

  return 1;
}

//----------------------------------------------------------------------------
void vtkCFSReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "File Name: " 
     << (this->FileName ? this->FileName : "(none)") << "\n";
}

//----------------------------------------------------------------------------
void vtkCFSReader::ReadFile(vtkMultiBlockDataSet *output)
{
  
#ifdef CFS_GUI
  // VERY important: As the reading in of the mesh could take som time
  // we want to call this method in a python thread.
  // This will only succeed, if we release the interpreter lock
  // and restore it in the end. For details refer to:
  //     http://stackoverflow.com/questions/8009613/boost-python-not-supporting-parallelism
  PyThreadState * saveState = PyEval_SaveThread();
#endif
  
  DBG_OUT( "==========================================\n"
      << " R E A D I N G   I N   F I L E \n"
      << "==========================================\n" );
  // ================================
  // The first time, the reader is initialized, we have to
  // fill the  multiblock data set
  // ================================
  if( !this->isInitialized ) {

    DBG_OUT( "==== Reading Grid for First Time ===");
    mbDataSet = vtkMultiBlockDataSet::New();
    mbDataSet->ShallowCopy(output);

    // Assemble number of blocks, which is 
    // #regions, #named elemens, #named nodes
    unsigned int numBlocks = regionNames_.size() + namedElemNames_.size()
                                   + namedNodeNames_.size();

    for( unsigned  int i = 0, n = numBlocks; i < n; i++ ) {
      vtkUnstructuredGrid * newGrid = vtkUnstructuredGrid::New();
      mbDataSet->SetBlock( i, newGrid );
      newGrid->Delete();
    }

    // initialize mapping from 
    // (region,globalNodeNumber)->regionLocalNodeNumber
    nodeMap_.resize( numBlocks );

    // read nodal connectivity
    this->ReadNodes( mbDataSet );

    // read element definition  
    this->ReadCells( mbDataSet );

    // Now the grid is finalized and we store it initially also 
    // to the activeSet
    this->mbActiveDataSet = vtkMultiBlockDataSet::New();
    this->mbActiveDataSet->ShallowCopy(this->mbDataSet);

    // now it should be definitly initialized
    this->isInitialized = true;

  } 
  

  // If region status has changed, we have to perform an update
  // of the active regions
  if( this->activeRegionsChanged || this->resetDataArrays ) {
    DBG_OUT("\t->Calling UpdateActiveRegions")
    DBG_OUT("this->activeRegionsChanged:" << this->activeRegionsChanged)
    DBG_OUT("this->resetDataArrays:" << this->resetDataArrays)
    this->UpdateActiveRegions();
  }

  // only read data, if time/frequency steps are defined
  if( this->NumberOfTimeSteps > 0 ) {
    // read nodal data
    DBG_OUT( "Reading nodal data");
    this->ReadNodeCellData( mbActiveDataSet, true);

    // read cell data
    DBG_OUT( "Reading cell data");
    this->ReadNodeCellData( mbActiveDataSet, false);
  }

  // Use the active set as base for the current set
  output->ShallowCopy(mbActiveDataSet);
  
#ifdef CFS_GUI
  // restore python thread state
  PyEval_RestoreThread(saveState);
#endif
}

//----------------------------------------------------------------------------
void vtkCFSReader::UpdateActiveRegions() {
  
  // Idea:
  // Create new multiblock data set, based on the internal stored one and copy only those
  // grid-blocs, which are active.
  
  vtkMultiBlockDataSet *newSet  = vtkMultiBlockDataSet::New();
  
  
  // =================================================
  //  Combine region and group info 
  // =================================================
  unsigned int numBlocks = this->regionNames_.size() + 
                           this->namedElemNames_.size() + 
                           namedNodeNames_.size();
  std::vector<std::string> names (numBlocks);
  std::map<std::string, int> switches;
  for( unsigned int iBlock = 0; iBlock < numBlocks; ++iBlock ) {
    if( iBlock < this->regionNames_.size() ) {
      std::string name = regionNames_[iBlock];
      names[iBlock] = name;
      switches[name] = regionSwitch[name];
    } else if (iBlock < this->regionNames_.size()+this->namedElemNames_.size() ) {
      unsigned index = iBlock - this->regionNames_.size();
      std::string name = namedElemNames_[index]; 
      names[iBlock] = name;
      switches[name] = namedElemSwitch[name];
    } else {
      unsigned index = iBlock - this->regionNames_.size()-this->namedElemNames_.size();
      std::string name = namedNodeNames_[index]; 
      names[iBlock] = name;
      switches[name] = namedNodeSwitch[name];
    }
  }
  
  // ============================================================
  //  Run over all active regions / element groups and copy grid
  // ============================================================
  unsigned int index = 0;
  for( unsigned int i = 0;i < numBlocks;  i++ ) {
    DBG_OUT("TREATING BLOCK" << i << " OF " << numBlocks)

    DBG_OUT("BLOCKNAME: " << names[i])

    // check, if region is active
    if( switches[names[i]] != 0 ) {
      DBG_OUT("\t ADDING BLOCK");

      // create new grid with output and make shallow copy
      vtkUnstructuredGrid * newGrid = vtkUnstructuredGrid::New();
      newGrid->ShallowCopy(mbDataSet->GetBlock(i));

      // try to find the corresponding block of this region/group in the current
      // active set in order to copy its data to this block.
      unsigned int numActBlocks = this->mbActiveDataSet->GetNumberOfBlocks();
      unsigned int activeIndex = 0;
      bool found = false;
      for( unsigned int j = 0; j < numActBlocks; ++j ) {
        const char * blockName = 
            this->mbActiveDataSet->GetMetaData(j)->Get(vtkCompositeDataSet::NAME());
        if( blockName != NULL ) {
          if( std::string(blockName) == names[i]) {
            found = true;
            activeIndex = j;
          }
        }
      }
      
      if( found && !this->resetDataArrays) {
        vtkUnstructuredGrid * actGrid = 
            vtkUnstructuredGrid::SafeDownCast(this->mbActiveDataSet->GetBlock(activeIndex));
        newGrid->GetPointData()->ShallowCopy( actGrid->GetPointData());
        newGrid->GetCellData()->ShallowCopy( actGrid->GetCellData());
      }
      
      // Get the array containing the actual node numbers for the current region
      newGrid->GetPointData()->SetActiveScalars("origNodeNums");
      vtkUnsignedIntArray *origNodeNums;
      origNodeNums = vtkUnsignedIntArray::SafeDownCast(newGrid->GetPointData()->GetScalars());
      newGrid->GetPointData()->SetActiveScalars("");
      

      // Get the array containing the actual node numbers for the current region / group
      newGrid->GetCellData()->SetActiveScalars("origElemNums");
      vtkUnsignedIntArray *origElemNums;
      origElemNums = vtkUnsignedIntArray::SafeDownCast(newGrid->GetCellData()->GetScalars());
      newGrid->GetCellData()->SetActiveScalars("");
      

      newSet->SetBlock(index, newGrid);
      
      // Tag new block with region name
      newSet->GetMetaData(index)->Set(vtkCompositeDataSet::NAME(), names[i].c_str());
      index++;

      newGrid->Delete();
    } // if block is active
  } //loop: all blocks (= regions, elem/node groups)
  
  // in the end, replace the active set by the current one
  this->mbActiveDataSet->ShallowCopy(newSet);
  newSet->Delete();

  // in the end, re-set the status of the update flag
  this->activeRegionsChanged = false;
  this->resetDataArrays = false;
}

//----------------------------------------------------------------------------
void vtkCFSReader::ReadNodeCellData(vtkMultiBlockDataSet *output, bool isNode)
{
  
  DBG_OUT("=========================");
  DBG_OUT( " Reading Node/Cell Data" );
  DBG_OUT("=========================");
  H5CFS::EntityType entType = H5CFS::ELEMENT;
  double h180deg_over_pi = 180.0 / pi;

  if( isNode ) {
    DBG_OUT("EntityType is NODE");
    entType = H5CFS::NODE;
  } else {
    DBG_OUT("EntityType is ELEMENT");
  }
  
  // =================================================
  //  Combine region and group info 
  // =================================================
  unsigned int numBlocks = this->regionNames_.size() + 
      this->namedElemNames_.size() + 
      namedNodeNames_.size();
  std::vector<std::string> names (numBlocks);
  std::map<std::string, int> switches;
  for( unsigned int iBlock = 0; iBlock < numBlocks; ++iBlock ) {
    if( iBlock < this->regionNames_.size() ) {
      std::string name = regionNames_[iBlock];
      names[iBlock] = name;
      switches[name] = regionSwitch[name];
    } else if (iBlock < this->regionNames_.size()+this->namedElemNames_.size() ) {
      unsigned index = iBlock - this->regionNames_.size();
      std::string name = namedElemNames_[index]; 
      names[iBlock] = name;
      switches[name] = namedElemSwitch[name];
    } else {
      unsigned index = iBlock - this->regionNames_.size()-this->namedElemNames_.size();
      std::string name = namedNodeNames_[index]; 
      names[iBlock] = name;
      switches[name] = namedNodeSwitch[name];
    }
  }
  // ===================================================

  
  try {
    if (meshResultInfos_.find(MultiSequenceStep) == meshResultInfos_.end()) {
      vtkWarningMacro("Multisequence step index "
                      + lexical_cast<std::string>(MultiSequenceStep)
                      + " is invalid.");
      return;
    }
    
    // get result infos for 1ms step and step 1
    DBG_OUT("Reading " << meshResultInfos_[MultiSequenceStep].size() << " result types");
    
    // Read all result types occuring in the current multisequence step
    std::set<std::string> resultNames;
    std::map<std::string, shared_ptr<H5CFS::ResultInfo> > nameInfo;
    for( unsigned int i = 0; i < meshResultInfos_[MultiSequenceStep].size(); ++i ) {
      std::string & resultName = meshResultInfos_[MultiSequenceStep][i]->name;
      resultNames.insert( resultName );
      nameInfo[resultName] = meshResultInfos_[MultiSequenceStep][i];
    }

    shared_ptr<H5CFS::Result> result;
    // ----------------------------
    //  Loop over all result names
    // -----------------------------
    std::set<std::string>::const_iterator it = resultNames.begin();
    for( ; it != resultNames.end(); ++it ) {
      std::string  resultName = *it;

      DBG_OUT("\tChecking Result : '" << resultName << '"');
      DBG_OUT("\tDefined On: '" << nameInfo[resultName]->listType << '"');
      
      // Ensure, that in the nodal case we only pick nodal results. 
      // In the case of element results, we accept elements and surface elements
      if( ( isNode && (nameInfo[resultName]->listType != H5CFS::NODE ) )
          ||(!isNode && (nameInfo[resultName]->listType != H5CFS::ELEMENT &&
                         nameInfo[resultName]->listType != H5CFS::SURF_ELEM) ) )
        continue;
      
      DBG_OUT( "\tReading Result : '" << resultName << "'" );
      
      // ------------------------------
      //  Loop over all active blocks
      // ------------------------------
      unsigned int blockIndex = 0;
      for( unsigned int iBlock = 0; iBlock < numBlocks; iBlock++ ) {

        // variable used to write the result 
        std::string  writeName = resultName;
        
        // check, if block is active. If not, just leave
        if( switches[names[iBlock]] == 0)
          continue;

        DBG_OUT( "\t\tReading Block: '" << names[iBlock] << "'");

        // get grip of grid of current block
        vtkUnstructuredGrid *actGrid = 
            vtkUnstructuredGrid::SafeDownCast( output->GetBlock(blockIndex++) );
        
        
        // -------------------------------------------------
        //  Find resultinfo for this result and this block
        // -------------------------------------------------
        bool found = false;
        shared_ptr<H5CFS::ResultInfo> actInfo;
        for( unsigned int i = 0; i < meshResultInfos_[MultiSequenceStep].size(); ++i ) {
          if( meshResultInfos_[MultiSequenceStep][i]->name == resultName &&
              meshResultInfos_[MultiSequenceStep][i]->listName == names[iBlock] &&
              meshResultInfos_[MultiSequenceStep][i]->listType == entType ) {
            actInfo = meshResultInfos_[MultiSequenceStep][i];
            found = true;
            DBG_OUT("\t\t=> Found");
            break;
          }
        }
        
        // if result for this block was not found and we should not generate
        // "empty" datasets for missing result, we just leave
        if( found == false ) {
          DBG_OUT("\t\t=> Not Found!");
          if (!FillMissingResults ) {
            DBG_OUT("Leaving");
            continue;
          } else {
            actInfo = nameInfo[resultName];
            DBG_OUT("\t\t => Filling missing results with zeros");
          }
        } 
        

        std::vector<unsigned int> entities;
        in_.GetEntities( entType, names[iBlock], entities );
        unsigned int numEntities = entities.size();
        unsigned int numDofs = actInfo->dofNames.size();
        std::vector<double> * ptRealVals = NULL;
        std::vector<double> * ptImagVals = NULL;
        bool isComplex = false;
        std::vector<double> dummyVals;
        if( found ) {
          result = shared_ptr<H5CFS::Result>(new H5CFS::Result());
          result->resultInfo = actInfo;
          try {
            in_.GetMeshResult( MultiSequenceStep, stepNums[TimeStep-1], result);
          } catch (...) {
            // If we are here, the current result is not defined on the current
            // step and we should NOT fill with empty results. In this case 
            // we perform the following:
            // a) if the current time/freq step is smaller than the first
            //    one the result is defined, we update to the first defined step
            // b) otherwise we simply return and keep the result from the
            //    previous steps
            std::map<unsigned int, double> steps;
            in_.GetStepValues(MultiSequenceStep, actInfo, steps, false);
            DBG_OUT("Step " << stepNums[TimeStep-1] << " not found. "
                    << "First step for result is " << steps.begin()->first);
            if(stepNums[TimeStep-1] < steps.begin()->first) {
              DBG_OUT("Step " << stepNums[TimeStep-1] << " missing. Taking "
                      << steps.begin()->first << " instead!");
              in_.GetMeshResult( MultiSequenceStep, steps.begin()->first, result);
            } else {
              // in this case we are in the middle of the steps and just keep
              // the old value
              break;
            }
          }
          isComplex = result->isComplex;
          numDofs = actInfo->dofNames.size();
          ptRealVals = &(result->realVals);
          ptImagVals = &(result->imagVals);
        } else {
          dummyVals.resize( numEntities * numDofs);
          ptRealVals = &dummyVals;
          ptImagVals = &dummyVals;
          // NOTE: we have to find a different strategy how to 
          // find out about complex valued results
          isComplex = (this->analysisType == H5CFS::HARMONIC);
        }
       
        std::vector<double> & realVals = *ptRealVals;

        //vtkDoubleArray *vals1 = NULL, *vals2 = NULL, *vals3 = NULL, *vals4 = NULL, *vals5 = NULL;
	std::list<vtkDoubleArray*> vals;

        // REAL-VALUED
        DBG_OUT( "\t\tCopying data to vtk array");
        if( !isComplex ) {
          if( ! (harmonicData && HarmonicDataAsModeShape) ) {
            DBG_OUT( "\t\tResult is REAL-VALUED");

            if(AddDimensionsToArrayNames) {
              writeName += " [";
              writeName += actInfo->unit;
              writeName += "]";
            }
            vals.push_back( SaveToArray( realVals, actInfo->dofNames, numEntities, writeName ) );
          } else { // We obviously have a real-valued eigenvalue solution
            //std::string freqNormed;
            unsigned int numEntries = realVals.size();

            std::string writeName = actInfo->name;
            writeName += "-mode";
            if(AddDimensionsToArrayNames) {
              writeName += " [";
              writeName += actInfo->unit;
              writeName += "]";
            }

            double reRot = cos(-2*pi*requestedTimeValue);

            for(unsigned int i = 0; i < numEntries; i++ ) {
              realVals[i] *= reRot;
            }

            vals.push_back( SaveToArray( realVals, actInfo->dofNames, numEntities, writeName ) );

          }
        } else {
          std::vector<double> & imagVals = *ptImagVals;
          DBG_OUT( "\t\tResult is COMPLEX");
          if( ComplexModeReal > 0) {
            std::string resName = actInfo->name + "Real";
            if(AddDimensionsToArrayNames) {
              resName += " [";
              resName += actInfo->unit;
              resName += "]";
            }
	    vals.push_back( SaveToArray( realVals, actInfo->dofNames, numEntities, resName ) );
          }
	  if( ComplexModeImag > 0) {
            std::string resName = actInfo->name + "Imag";
            if(AddDimensionsToArrayNames) {
              resName += " [";
              resName += actInfo->unit;
              resName += "]";
            }
	    vals.push_back( SaveToArray( imagVals, actInfo->dofNames, numEntities, resName ) );
          }
	  if ( ComplexModeAmpl > 0 ) {
            unsigned int numEntries = realVals.size();
            std::string amplName = actInfo->name + "Ampl";
            if(AddDimensionsToArrayNames) {
              amplName += " [";
              amplName += actInfo->unit;
              amplName += "]";
            }
            std::vector<double> ampl(numEntries);
            for(unsigned int i = 0; i < numEntries; i++ ) {
              ampl[i] = hypot( realVals[i], imagVals[i] );
            }
	    vals.push_back( SaveToArray( ampl, actInfo->dofNames, numEntities, amplName ) );
          }
	  if ( ComplexModePhase > 0 ) {
            unsigned int numEntries = realVals.size();
            std::string phaseName = actInfo->name + "Phase";
            if(AddDimensionsToArrayNames) {
              phaseName += " [deg]";
            }
            std::vector<double> phase(numEntries);
            for(unsigned int i = 0; i < numEntries; i++ ) {
              phase[i] = (std::abs(imagVals[i]) > 1e-16) ?                   
                  std::atan2( imagVals[i], realVals[i] ) * h180deg_over_pi : 
                  ( realVals[i] < 0.0 ) ? 180 : 0 ; 
            }
	    vals.push_back( SaveToArray( phase, actInfo->dofNames, numEntities, phaseName ) );
          }
          // Compute time continuous field from harmonic data by multiplying complex
          // result field with rotating phasor
          if ( HarmonicDataAsModeShape ) {
            unsigned int numEntries = realVals.size();
            std::string writeName = actInfo->name;
            if(AddDimensionsToArrayNames) {
              writeName += " [";
              writeName += actInfo->unit;
              writeName += "]";
            }
            std::vector<double> modeShape(numEntries);
            // Compute the rotation pointer from the requestedTimeValue which is in the
            // interval [0,1]
            double reRot = cos(2*pi*requestedTimeValue);
            double imRot = sin(2*pi*requestedTimeValue);
            for(unsigned int i = 0; i < numEntries; i++ ) {
              // Just compute the real part of the field
              modeShape[i] = realVals[i] * reRot - imagVals[i] * imRot;
            }
            //vals5 = SaveToArray( modeShape, actInfo->dofNames, numEntities, writeName );
	    vals.push_back( SaveToArray( modeShape, actInfo->dofNames, numEntities, writeName ) );
          } // harmonic data as mode shape
        } // if isComplex

        // save values to grid
        if( entType == H5CFS::NODE) { 
          DBG_OUT( "\t\tPassing nodal result to grid");
	  for(std::list<vtkDoubleArray *>::iterator it=vals.begin();it!=vals.end();++it)
          {
	    actGrid->GetPointData()->AddArray(*it);
          }   
        } else {
          DBG_OUT( "\t\tPassing element result to grid");
	  for(std::list<vtkDoubleArray *>::iterator it=vals.begin();it!=vals.end();++it)
          {
	    actGrid->GetCellData()->AddArray(*it);
          }  	  
        }
        // delete values (de-crement reference counter)
        DBG_OUT("\t\tCleaning up pointer data");
        for(std::list<vtkDoubleArray *>::iterator it=vals.begin();it!=vals.end();++it)
        {
          (*it)->Delete();
        }
      } // loop over active blocks
    } // loop over result types
  } catch(std::exception& ex ) {
    vtkErrorMacro(<< "Caught exception when reading results: '" 
                  << ex.what() << "'");
  } catch(std::string& ex ) {
    vtkErrorMacro(<< "Caught exception when reading results: '" 
                  << ex << "'");
  }

}

vtkDoubleArray* vtkCFSReader::SaveToArray( const std::vector<double>& vals,
                                            const std::vector<std::string>& dofNames,
                                           unsigned int numEntities,
                                           const std::string& name ) {
  
  vtkDoubleArray * ret = vtkDoubleArray::New();
  unsigned int numDofs = dofNames.size();
  // Case 1: numDofs is 1 (scalar) or >= 3 (real vector/tensor)
  if( numDofs == 1 || numDofs >= 3) {
    ret->SetNumberOfComponents( numDofs );
    for( unsigned int i = 0; i < numDofs; ++i ) {
      ret->SetComponentName(i, dofNames[i].c_str());
    }
    ret->SetNumberOfTuples( numEntities);
    ret->SetName( name.c_str() );
    double * retPtr = ret->GetPointer(0);
    unsigned int numEntries = numDofs * numEntities;
    for( unsigned int j = 0; j < numEntries; j++ ) { 
      retPtr[j] = vals[j];
    }
  } else {
    // Case 2: numDofs (vector field in 2D)
    // -> add artificial 3rd component, which is 0
    ret->SetNumberOfComponents( 3 );
    for( unsigned int i = 0; i < numDofs; ++i ) {
      ret->SetComponentName(i, dofNames[i].c_str());
    }
    ret->SetComponentName(2, "");
    ret->SetNumberOfTuples( numEntities );
    ret->SetName( name.c_str() );
    double * retPtr = ret->GetPointer(0);
    unsigned int index = 0;
    for( unsigned int iEnt = 0; iEnt < numEntities; iEnt++ ) { 
      retPtr[index++] = vals[iEnt*2+0];
      retPtr[index++] = vals[iEnt*2+1];
      retPtr[index++] = 0.0;
    }
  }
  return ret;
  
  // Alternative, slower way of filling a double array
  // =================================================
  //      std::vector<double> tuple (numDofs);
  //      for( unsigned int e = 0; e < entities.size(); e++ ) {
  //        for( unsigned int d = 0; d < numDofs; d++ ) {
  //          tuple[d] = result->realVals[numDofs*e+d];
  //        }
  //vals->SetTuple(e, &tuple[0]);
  //vals->SetArray(&(result->realVals[0]), 
  //               result->realVals.size(), 1);
  //}
}





//----------------------------------------------------------------------------
void vtkCFSReader::ReadCells(vtkMultiBlockDataSet *output)
{

  std::vector<H5CFS::ElemType> types;
  std::vector<std::vector<unsigned int> > connect;
  DBG_OUT("In ReadCells")
  
  try {
    in_.GetElems( types, connect );
    vtkDebugMacro(<< "Read in the element definitions");

    // unsigned int numElems = connect.size();
    // =======================
    //  loop over all regions
    // =======================
    DBG_OUT("I) Adding region elements")  
    for( unsigned int iRegion = 0, n = regionNames_.size(); 
    iRegion < n; iRegion++ ) {

      std::vector<unsigned int> regionElems;
      DBG_OUT("\tregion:" << regionNames_[iRegion] << "(block: " << iRegion << ")");
      in_.GetElemsOfRegion( regionNames_[iRegion], regionElems);
      unsigned int numRegionElems = regionElems.size();

      //vtkDebugMacro(<< "Reading region '" << regionNames_[iRegion] << "'");

      vtkUnstructuredGrid *actGrid = 
        vtkUnstructuredGrid::SafeDownCast( output->GetBlock(iRegion) );
      
      this-> AddElements( actGrid, iRegion, regionElems, types, 
                          connect );
    }
   
    unsigned int offset = regionNames_.size();

    // ==============================
    //  loop over all element groups
    // ==============================
    DBG_OUT("II) Adding named elements")  
    for( unsigned int iGroup = 0, n = namedElemNames_.size(); 
        iGroup < n; iGroup++ ) {

      std::string groupName = namedElemNames_[iGroup];
      DBG_OUT("\tgroup:" << groupName << "(block: " << iGroup+offset << ")");
      std::vector<unsigned int> groupElems;
      in_.GetNamedElems( groupName, groupElems);
      unsigned int numGroupElems = groupElems.size();

      //vtkDebugMacro(<< "Reading region '" << regionNames_[iRegion] << "'");

      vtkUnstructuredGrid *actGrid = 
          vtkUnstructuredGrid::SafeDownCast( output->GetBlock(iGroup+offset) );
      this-> AddElements( actGrid, iGroup+offset, groupElems, types, 
                          connect);
    }

    offset += namedElemNames_.size();
    
    // ==============================
    //  loop over all nodal groups
    // ==============================
    
    DBG_OUT("III) Adding named nodes")
    unsigned int vertexElemNum = in_.GetNumElems(1) + in_.GetNumElems(2) + in_.GetNumElems(3);
    for( unsigned int iGroup = 0, n = namedNodeNames_.size(); 
        iGroup < n; iGroup++ ) {

      std::string groupName = namedNodeNames_[iGroup];
      DBG_OUT("\tgroup:" << groupName << "(block: " << iGroup+offset << ")");
      std::vector<unsigned int> groupNodes;
      in_.GetNamedNodes( groupName, groupNodes);
      unsigned int numGroupNodes = groupNodes.size();
      
      // setup array with virtual element numbers and virtual connectivity
      std::vector<unsigned int> vElemNums( numGroupNodes );
      std::vector<std::vector<unsigned int> > vElemConnect( numGroupNodes );
      std::vector<H5CFS::ElemType> vElemType( numGroupNodes );
      for( unsigned int iNode = 0; iNode < numGroupNodes; ++iNode ) {
        vElemNums[iNode] = iNode+1;
        vElemConnect[iNode].resize(1);
        vElemConnect[iNode][0] = groupNodes[iNode];
        vElemType[iNode] = H5CFS::ET_POINT;
      }
      //vtkDebugMacro(<< "Reading region '" << regionNames_[iRegion] << "'");

      vtkUnstructuredGrid *actGrid = 
          vtkUnstructuredGrid::SafeDownCast( output->GetBlock(iGroup+offset) );
      this-> AddElements( actGrid, iGroup+offset, vElemNums, vElemType, 
                          vElemConnect);
    }

  } catch(std::exception& ex ) {
    vtkErrorMacro(<< "Caught exception when reading cells: '" 
                  << ex.what() << "'");
  } catch(std::string& ex ) {
    vtkErrorMacro(<< "Caught exception when reading cells: '" 
                  << ex << "'");
  }
}


void vtkCFSReader::AddElements( vtkUnstructuredGrid *actGrid, 
                                 unsigned int blockIndex,
                                 const std::vector<unsigned int>&  elems,
                                 std::vector<H5CFS::ElemType>& types,
                                 std::vector<std::vector<unsigned int> >& connect ) {
  VTKCellType elemType = VTK_EMPTY_CELL;
  try {
    unsigned int numElems = elems.size();
    DBG_OUT("Adding " << numElems << " elements")
    actGrid->Allocate( numElems );

    // Add a result vector containing the original element numbers
    // to the current unstructured grid.
    vtkUnsignedIntArray * origElemNums = vtkUnsignedIntArray::New();
    origElemNums->SetNumberOfValues( numElems );
    origElemNums->SetName( "origElemNums" );
    actGrid->GetCellData()->AddArray(origElemNums);

    // Add a result vector containing the CFS element types
    // to the current unstructured grid.
    DBG_OUT("Adding array 'elemTypes'")
    vtkUnsignedIntArray * elemTypes = vtkUnsignedIntArray::New();
    elemTypes->SetNumberOfValues( numElems );
    elemTypes->SetName( "elemTypes" );
    actGrid->GetCellData()->AddArray(elemTypes);

    // loop over all elements
    for( unsigned int i = 0; i < numElems; i++ ) {

      unsigned int actElem = elems[i];
      DBG_OUT("Treating element #" << actElem )
      
      origElemNums->SetValue(i, actElem);
      elemTypes->SetValue(i, types[actElem-1]);

      // create correct vtk element
      elemType = GetCellIdType( types[actElem-1] );
      if( elemType == VTK_EMPTY_CELL ) {
        vtkErrorMacro(<<"FE type " << types[actElem-1] << " not implemented yet");
      }
      // set connectivity
      vtkIdType nodeIds[27];
      DBG_OUT("Adding " << connect[actElem-1].size() << "nodes")
      for(unsigned int j = 0, n = connect[actElem-1].size(); j < n; j++)
      {
         vtkDebugMacro(<< "addding nodeNum" << connect[actElem-1][j] );
        nodeIds[j] = nodeMap_[blockIndex][connect[actElem-1][j]-1]-1;
      }
      DBG_OUT("Elem type is " << elemType)
      if(elemType == VTK_TRIQUADRATIC_HEXAHEDRON) 
      {
        // top 
        //   7--14--6
        //   |      |
        //  15  25  13
        //   |      |
        //   4--12--5

        //   middle
        //  19--23--18
        //   |      |
        //  20  26  21
        //   |      |
        //  16--22--17

        //  bottom
        //   3--10--2
        //   |      |
        //  11  24  9 
        //   |      |
        //   0-- 8--1

        nodeIds[20] = nodeMap_[blockIndex][connect[actElem-1][23]-1]-1;
        nodeIds[21] = nodeMap_[blockIndex][connect[actElem-1][21]-1]-1;
        nodeIds[22] = nodeMap_[blockIndex][connect[actElem-1][20]-1]-1;
        nodeIds[23] = nodeMap_[blockIndex][connect[actElem-1][22]-1]-1;
      }

      // unsigned int id = 
      //DBG_OUT("Inserting cell to grid)")
      actGrid->InsertNextCell( elemType, connect[actElem-1].size(),nodeIds );
    } // loop: elements
  } catch(std::exception& ex ) {
    vtkErrorMacro(<< "Caught exception when reading cells: '" 
                  << ex.what() << "'");
  } catch(std::string& ex ) {
    vtkErrorMacro(<< "Caught exception when reading cells: '" 
                  << ex << "'");
  }
}

//----------------------------------------------------------------------------
void vtkCFSReader::ReadNodes(vtkMultiBlockDataSet *output)
{

  try {
    // get all nodal coordinates
    std::vector<std::vector<double> > coords;
    in_.GetNodeCoords( coords );


    // loop over all regions
    for( unsigned int iRegion = 0, n = regionNames_.size(); 
    iRegion < n; iRegion++ ) {
      nodeMap_[iRegion].resize( coords.size() );
      std::vector<unsigned int> regionNodes;
      in_.GetNodesOfRegion( regionNames_[iRegion], regionNodes);
      unsigned int numRegionNodes = regionNodes.size();

      vtkUnstructuredGrid *actGrid = 
        vtkUnstructuredGrid::SafeDownCast( output->GetBlock(iRegion) );

      vtkPoints* points = vtkPoints::New();
      points->SetNumberOfPoints( numRegionNodes );
      
      // Add a result vector containing the original node numbers
      // to the current unstructured grid.
      vtkUnsignedIntArray * origNodeNums = vtkUnsignedIntArray::New();
      origNodeNums->SetNumberOfValues( numRegionNodes );
      origNodeNums->SetName( "origNodeNums" );
      actGrid->GetPointData()->AddArray(origNodeNums);

      for( unsigned int i = 0; i < numRegionNodes; i++ ) {
        const std::vector<double> & p = coords[regionNodes[i]-1];
        origNodeNums->SetValue(i, regionNodes[i]);
        nodeMap_[iRegion][regionNodes[i]-1] = i+1;
        points->SetPoint(i, p[0], p[1], p[2]);
      }
      actGrid->SetPoints( points );
      points->Delete();
    }
    
    // loop over all element groups and set up nodal mapping
    unsigned int pos = regionNames_.size();
    for( unsigned int iGroup = 0, n = namedElemNames_.size();
        iGroup < n; iGroup++, pos++ ) {
      
      std::string groupName = namedElemNames_[iGroup];
      DBG_OUT("Setting up nodal connectivity for element group ' " << groupName << "'");
      DBG_OUT("Block index: " << pos)
      std::vector<unsigned int> groupNodes;
      in_.GetNamedNodes( groupName, groupNodes);
      DBG_OUT("Number of nodes:" << groupNodes.size())
      

      nodeMap_[pos].resize( coords.size() );
      unsigned int numGroupNodes = groupNodes.size();

      vtkUnstructuredGrid *actGrid = 
          vtkUnstructuredGrid::SafeDownCast( output->GetBlock(pos) );

      vtkPoints* points = vtkPoints::New();
      points->SetNumberOfPoints( numGroupNodes );

      // Add a result vector containing the original node numbers
      // to the current unstructured grid.
      vtkUnsignedIntArray * origNodeNums = vtkUnsignedIntArray::New();
      origNodeNums->SetNumberOfValues( numGroupNodes );
      origNodeNums->SetName( "origNodeNums" );
      actGrid->GetPointData()->AddArray(origNodeNums);

      for( unsigned int i = 0; i < numGroupNodes; i++ ) {
        const std::vector<double> & p = coords[groupNodes[i]-1];
        origNodeNums->SetValue(i, groupNodes[i]);
        nodeMap_[pos][groupNodes[i]-1] = i+1;
        points->SetPoint(i, p[0], p[1], p[2]);
      }
      actGrid->SetPoints( points );
      points->Delete();
    }
    
    // loop over all nodal groups and set up nodal mapping
    for( unsigned int iGroup = 0, n = namedNodeNames_.size();
        iGroup < n; iGroup++, pos++ ) {
      
      std::string groupName = namedNodeNames_[iGroup];
      DBG_OUT("Setting up nodal connectivity for nodalgroup ' " << groupName << "'");
      DBG_OUT("Block index: " << pos)
      std::vector<unsigned int> groupNodes;
      in_.GetNamedNodes( groupName, groupNodes);
      DBG_OUT("Number of nodes:" << groupNodes.size())
      

      nodeMap_[pos].resize( coords.size() );
      unsigned int numGroupNodes = groupNodes.size();

      vtkUnstructuredGrid *actGrid = 
          vtkUnstructuredGrid::SafeDownCast( output->GetBlock(pos) );

      vtkPoints* points = vtkPoints::New();
      points->SetNumberOfPoints( numGroupNodes );

      // Add a result vector containing the original node numbers
      // to the current unstructured grid.
      vtkUnsignedIntArray * origNodeNums = vtkUnsignedIntArray::New();
      origNodeNums->SetNumberOfValues( numGroupNodes );
      origNodeNums->SetName( "origNodeNums" );
      actGrid->GetPointData()->AddArray(origNodeNums);

      for( unsigned int i = 0; i < numGroupNodes; i++ ) {
        const std::vector<double> & p = coords[groupNodes[i]-1];
        origNodeNums->SetValue(i, groupNodes[i]);
        nodeMap_[pos][groupNodes[i]-1] = i+1;
        points->SetPoint(i, p[0], p[1], p[2]);
      }
      actGrid->SetPoints( points );
      points->Delete();
    }
    
  } catch(std::exception& ex ) {
    vtkErrorMacro(<< "Caught exception when reading nodes: '" 
                   << ex.what() << "'");
  } catch(std::string& ex ) {
    vtkErrorMacro(<< "Caught exception when reading nodes: '" 
                   << ex << "'");
  }

  vtkDebugMacro(<< "Finished reading nodes");

}

VTKCellType  vtkCFSReader::GetCellIdType( H5CFS::ElemType cfsType) {
  VTKCellType type =  VTK_EMPTY_CELL;
  switch( cfsType ) {
    case H5CFS::ET_UNDEF:
      break;
    case H5CFS::ET_POINT:
      type = VTK_VERTEX;
      break;
    case H5CFS::ET_LINE2:
      type = VTK_LINE;
      break;
    case H5CFS::ET_LINE3:
      type = VTK_QUADRATIC_EDGE;
      break;
    case H5CFS::ET_TRIA3:
      type = VTK_TRIANGLE;
      break;
    case H5CFS::ET_TRIA6:
      type = VTK_QUADRATIC_TRIANGLE;
      break;
    case H5CFS::ET_QUAD4:
      type = VTK_QUAD;
      break;
    case H5CFS::ET_QUAD8:
      type = VTK_QUADRATIC_QUAD;
      break;
    case H5CFS::ET_QUAD9:
      type = VTK_BIQUADRATIC_QUAD;
      break;
    case H5CFS::ET_TET4:
      type = VTK_TETRA;
      break;
    case H5CFS::ET_TET10:
      type = VTK_QUADRATIC_TETRA;
      break;
    case H5CFS::ET_HEXA8:
      type = VTK_HEXAHEDRON;
      break;
    case H5CFS::ET_HEXA20:
      type = VTK_QUADRATIC_HEXAHEDRON;
      break;
    case H5CFS::ET_HEXA27:
      type = VTK_TRIQUADRATIC_HEXAHEDRON;
      break;
    case H5CFS::ET_PYRA5:
      type = VTK_PYRAMID;
      break;
    case H5CFS::ET_PYRA13:
      type = VTK_QUADRATIC_PYRAMID;
      break;
    case H5CFS::ET_PYRA14:
      type = VTK_QUADRATIC_PYRAMID;
      break;
    case H5CFS::ET_WEDGE6:
      type = VTK_WEDGE;
      break;
    case H5CFS::ET_WEDGE15:
      type = VTK_QUADRATIC_WEDGE;
      break;
    case H5CFS::ET_WEDGE18:
      type = VTK_BIQUADRATIC_QUADRATIC_WEDGE;
      break;
  }
  return type;
      
 }

const char* freqSuffixes[] = 
{"fHz", "nHz", "muHz", "mHz", "Hz", "kHz", "MHz", "GHz", "THz", "PHz", "EHz", "Zhz", "YHz"};
 
void vtkCFSReader::PrettyPrintFreq(double freq, std::string& val) {
  
  // explicitly check for freq = 0
  if( std::abs(freq) < 1e-16) {
    val = "0.0 s";
    return;
  }
  
  int expo3 = int(floor(log10(std::abs(freq))/3));
  double reduced = freq/pow(double(10.0),expo3*3.0);
  boost::format fmt ("%3.3f %s");
  fmt % reduced % freqSuffixes[expo3+4];
  val = fmt.str();
}

const char* timeSuffixes[] = {"fs", "ns", "mus", "ms", "s"};

void vtkCFSReader::PrettyPrintTime(double time, std::string& val) {

  // explicitly check for time = 0
  if( std::abs(time) < 1e-16) {
    val = "0.0 s";
    return;
  }
    
  int expo3 = int(floor(log10(std::abs(time))/3));
  double reduced = time/pow(double(10.0),expo3*3);
  boost::format fmt ("%3.3f %s");
  fmt % reduced % timeSuffixes[expo3+4];
  val = fmt.str();
}

vtkInformationVector* vtkCFSReader::GetMultiSequenceInfo(bool history) {

  // Read basic information from HDF5
  ReadHdf5Info();
  
  // create new vtkInformation vector that will take the multisequence infos
  vtkInformationVector *msInfo = vtkInformationVector::New();
  
  std::map<unsigned int, H5CFS::AnalysisType>::const_iterator msIt, msEnd;
  if (history) {
    msIt = histAnalysisType_.begin();
    msEnd = histAnalysisType_.end();
  }
  else {
    msIt = resAnalysisType_.begin();
    msEnd = resAnalysisType_.end();
  }
  
  for ( ; msIt != msEnd; ++msIt) {
    // create new vtkInformation object
    vtkInformation *stepInfo = vtkInformation::New();

    // store sequence step index and analysis type
    stepInfo->Set(MULTI_SEQ_INDEX(), msIt->first);
    stepInfo->Set(ANALYSIS_TYPE(), msIt->second);
    
    if (!history) {
      if (globalResultSteps_[msIt->first].size() == 0) {
        std::vector< shared_ptr<H5CFS::ResultInfo> >::iterator
            resIt = meshResultInfos_[msIt->first].begin(),
            resEnd = meshResultInfos_[msIt->first].end();
        std::map<unsigned int, double> steps;
        for ( ; resIt != resEnd; ++resIt) {
          in_.GetStepValues(msIt->first, *resIt, steps, false);
          
          std::map<unsigned int, double>::iterator stepIt = steps.begin(),
              stepEnd = steps.end();
          for ( ; stepIt != stepEnd; ++stepIt) {
            globalResultSteps_[msIt->first].insert(
                std::pair<unsigned int, double>(stepIt->first, stepIt->second));
          }
        }
      }
      
      std::set< std::pair<unsigned int, double> >::iterator
          stepIt = globalResultSteps_[msIt->first].begin(),
          stepEnd = globalResultSteps_[msIt->first].end();
      for ( ; stepIt != stepEnd; ++stepIt) {
        stepInfo->Append(STEP_NUMS(), stepIt->first);
        stepInfo->Append(STEP_VALUES(), stepIt->second);
      }
    }
    
    // append vtkInformation object to vtkInformationVector
    msInfo->Append(stepInfo);
  }
  
  return msInfo;
}

vtkInformationVector* vtkCFSReader::GetResultInfo(unsigned int msStep,
                                                   bool history)
{
  // Read basic information from HDF5
  ReadHdf5Info();
  
  // create new vtkInformation vector that will take the result infos
  vtkInformationVector *vtkInfoVec = vtkInformationVector::New();
  
  // select ResultInfos for history or mesh results?
  std::map<unsigned int, std::vector< shared_ptr<H5CFS::ResultInfo> > >
      &resInfoMap = history ? histResultInfos_ : meshResultInfos_;
  
  // make sure that the multisequence index is valid
  if (resInfoMap.find(msStep) == resInfoMap.end()) {
    vtkWarningMacro("Multisequence step index "
                    + lexical_cast<std::string>(msStep) + " is invalid.");
    return vtkInfoVec;
  }
  
  // loop over history results
  unsigned int iRes, numResults = resInfoMap[msStep].size();
  for (iRes=0; iRes<numResults; ++iRes) {
    
    // create new vtkInformation object
    vtkInformation *vtkInfo = vtkInformation::New();

    shared_ptr<H5CFS::ResultInfo> resInfo = resInfoMap[msStep][iRes];
    
    // copy scalar/string data into vtkInformation object
    vtkInfo->Set(RESULT_NAME(), resInfo->name.c_str());
    vtkInfo->Set(DEFINED_ON(), resInfo->listType);
    vtkInfo->Set(ENTITY_NAME(), resInfo->listName.c_str());
    vtkInfo->Set(ENTRY_TYPE(), resInfo->entryType);
    vtkInfo->Set(UNIT(), resInfo->unit.c_str());

    // fill vector of DOF names
    for (int iDof=0, numDofs=resInfo->dofNames.size(); iDof<numDofs; ++iDof) {
      vtkInfo->Append(DOF_NAMES(), resInfo->dofNames[iDof].c_str());
    }
    
    // fill step numbers and values
    std::map<unsigned int, double> steps;
    in_.GetStepValues(msStep, resInfo, steps, history);
    std::map<unsigned int, double>::const_iterator stepIt = steps.begin(),
                                                   stepEnd = steps.end();
    for ( ; stepIt != stepEnd; ++stepIt) {
      vtkInfo->Append(STEP_NUMS(), stepIt->first);
      vtkInfo->Append(STEP_VALUES(), stepIt->second);
    }

    // append vtkInformation object to vtkInformationVector
    vtkInfoVec->Append(vtkInfo);
  }
  
  return vtkInfoVec;
}

vtkInformation* vtkCFSReader::GetEntityIds(vtkInformation *result) {

  // Read basic information from HDF5
  ReadHdf5Info();
  
  // make sure that vtkInformation object contains required infos 
  if (!result->Has(DEFINED_ON())) {
    vtkErrorMacro("Required input member DEFINED_ON is missing.");
  }
  if (!result->Has(ENTITY_NAME())) {
    vtkErrorMacro("Required input member ENTITY_NAME is missing.");
  }
  
  // extract data from vtkInformation object
  int definedOn = result->Get(DEFINED_ON());
  std::string entityName = result->Get(ENTITY_NAME());
  vtkInformation *entityIds = vtkInformation::New();
  
  // if the result is defined on nodes or element, we just convert node/element
  // numbers into strings
  if ((definedOn == H5CFS::NODE) || (definedOn == H5CFS::ELEMENT)
      || (definedOn == H5CFS::SURF_ELEM)) {
    std::vector<unsigned int> entities;
    in_.GetEntities((H5CFS::EntityType) definedOn, entityName, entities);
    for (int i=0, n=entities.size(); i<n; ++i) {
      entityIds->Append(ENTITY_IDS(),
                        lexical_cast<std::string>(entities[i]).c_str());
    }
  } else {
    // in all other cases the ID is the entity name itself
    entityIds->Append(ENTITY_IDS(), entityName.c_str());
  }
    
  return entityIds;
}

void vtkCFSReader::GetHistoryResult(unsigned int msStep,
                                     vtkInformation *histResult,
                                     char *entityId,
                                     vtkDoubleArray *realPart,
                                     vtkDoubleArray *imagPart)
{

  // Read basic information from HDF5
  ReadHdf5Info();
  
  // make sure that the multisequence index is valid
  if (histResultInfos_.find(msStep) == histResultInfos_.end()) {
    vtkErrorMacro("Multisequence step index "
                    + lexical_cast<std::string>(msStep) + " is invalid.");
  }
  
  // make sure that vtkInformation object contains required infos 
  if (!histResult->Has(RESULT_NAME())) {
    vtkErrorMacro("Required input member RESULT_NAME is missing.");
  }
  if (!histResult->Has(ENTITY_NAME())) {
    vtkErrorMacro("Required input member ENTITY_NAME is missing.");
  }
  
  // extract data from vtkInformation object
  std::string resName = histResult->Get(RESULT_NAME());
  std::string entityName = histResult->Get(ENTITY_NAME());
  
  // create storage space for result
  shared_ptr<H5CFS::Result> result(new H5CFS::Result());

  // make sure that the result actually exists in that multisequence step
  unsigned int i, numResults = histResultInfos_[msStep].size();
  for (i = 0; i < numResults; ++i) {
    if (histResultInfos_[msStep][i]->name == resName &&
        histResultInfos_[msStep][i]->listName == entityName)
    {
      result->resultInfo = histResultInfos_[msStep][i];
      break;
    }
  }
  if (i == numResults) {
    vtkErrorMacro("Could not find result '" + resName + "' on group '"
                  + entityName + "'.");
  }
  
  // Load history result
  in_.GetHistResult(msStep, entityId, result);
  
  // get array dimensions
  unsigned int numDofs = result->resultInfo->dofNames.size();
  unsigned int numValues = result->realVals.size();
  
  // initialize result buffer and obtain pointer
  realPart->SetNumberOfComponents(numDofs);
  realPart->SetNumberOfTuples(numValues / numDofs);
  double *realPtr = realPart->GetPointer(0);
  
  // copy result into buffer
  for (unsigned int i = 0; i < numValues; ++i) {
    realPtr[i] = result->realVals[i];
  }

  // do the same for imaginary part if necessary
  if (result->isComplex) {
    if (histAnalysisType_[msStep] == H5CFS::HARMONIC ||
        histAnalysisType_[msStep] == H5CFS::EIGENVALUE ||
        histAnalysisType_[msStep] == H5CFS::EIGENFREQUENCY)
    {
      imagPart->SetNumberOfComponents(numDofs);
      imagPart->SetNumberOfTuples(numValues / numDofs);
      double *imagPtr = imagPart->GetPointer(0);
      for (unsigned int i = 0; i < numValues; ++i) {
        imagPtr[i] = result->imagVals[i];
      }
    }
    else {
      vtkWarningMacro("Result '" + resName
          + "' has an imaginary part although it should be real-valued.");
    }
  }
  else {
    if (histAnalysisType_[msStep] == H5CFS::HARMONIC ||
	histAnalysisType_[msStep] == H5CFS::EIGENVALUE ||
        histAnalysisType_[msStep] == H5CFS::EIGENFREQUENCY)
    {
      vtkWarningMacro("Result '" + resName
          + "' is real-valued although it should be complex.");
    }
  }
}

void vtkCFSReader::GetNodeCoords(vtkDoubleArray *vtkCoords) {

  // Read basic information from HDF5
  ReadHdf5Info();
  
  std::vector< std::vector<double> > coords;
  in_.GetNodeCoords(coords);
  unsigned int numNodes = coords.size();
  
  vtkCoords->SetNumberOfComponents(3);
  vtkCoords->SetNumberOfTuples(numNodes);
  
  if (vtkCoords->HasStandardMemoryLayout()) {
    double *vtkPtr = vtkCoords->GetPointer(0);
    unsigned int idx = 0;
    
    for (unsigned int i=0; i<numNodes; ++i) {
      vtkPtr[idx  ] = coords[i][0];
      vtkPtr[idx+1] = coords[i][1];
      vtkPtr[idx+2] = coords[i][2];
      idx += 3;
    }
  }
  else {
    vtkErrorMacro(<< "Contiguous memory layout is required.");
  }
}
