/** This is the reader for HDF5 files from CFS. */
#ifndef __vtkCFSReader_h
#define __vtkCFSReader_h

#include <vector>
#include "vtkSetGet.h"
#include "vtkObjectBase.h"
#include "vtkWin32Header.h"
#include "vtkMultiBlockDataSetAlgorithm.h"
#include "vtkUnstructuredGridAlgorithm.h"
#include "vtkCellType.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkInformationIntegerKey.h"
#include "vtkInformationStringKey.h"
#include "vtkInformationDoubleVectorKey.h"
#include "vtkInformationIntegerVectorKey.h"
#include "vtkInformationStringVectorKey.h"
#include "vtkInformationVector.h"

#include "../hdf5Reader.h"

#include "vtkCFSReaderModule.h"


class vtkDoubleArray;
class vtkCell;

class VTKCFSREADER_EXPORT vtkCFSReader : public vtkMultiBlockDataSetAlgorithm
{
public:
  static vtkCFSReader *New();
  vtkTypeMacro(vtkCFSReader,vtkMultiBlockDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Check, if the current file is really a CFS hdf5 file.
  
  // This method gets called in order to determine the "right" reader in case
  // several readers can open files with the same extension.
  virtual int CanReadFile(const char* fname);
  
  // Description:
  // Specify the file name of the CFS data file to read.
  virtual void SetFileName(const char* _arg);
  
  // Description:
  // Close then file handle
  virtual void CloseFile();
  
  //vtkSetStringMacro(FileName);
  vtkGetStringMacro(FileName);

  // Description:
  // Get the multisequence range. Filled during RequestInformation.
  vtkGetVector2Macro(MultiSequenceRange, int);

  // Description:
  // Set/Get the current multisequence step
  void SetMultiSequenceStep(int step);
  //vtkSetMacro(MultiSequenceStep, int);
  vtkGetMacro(MultiSequenceStep, int);
  
  // Description:
  // Set/Get the current timestep.
  virtual void SetTimeStep(unsigned int step);
  vtkGetMacro(TimeStep, unsigned int);
  
  // Description:
  // Get current frequency / time value
  virtual const char* GetTimeFreqValStr(); 

  // Description:
  // Add dimensions to array names
  virtual void SetAddDimensionsToArrayNames(int);
  vtkGetMacro(AddDimensionsToArrayNames, int);
  vtkBooleanMacro(AddDimensionsToArrayNames, int);

  // Description:
  // Add dimensions to array names
  virtual void SetHarmonicDataAsModeShape(int);
  vtkGetMacro(HarmonicDataAsModeShape, int);
  vtkBooleanMacro(HarmonicDataAsModeShape, int);

  // Description:
  // Return the number of time / frequency steps
  virtual unsigned int GetNumSteps() {
    return this->NumberOfTimeSteps; 
  }
  
  // Description:
  // Get the timesteprange. Filled during RequestInformation.
  vtkGetVector2Macro(TimeStepNumRange, int);
 
  virtual double *GetTimeStepRange ()
  {
    vtkDebugMacro(<< this->GetClassName() << " (" << this << "): returning TimeStepRange pointer "
                  << this->TimeStepValuesRange);
    
    return this->TimeStepValuesRange;
  }
  
  virtual void GetTimeStepRange (double &_arg1, double &_arg2)
  {
    _arg1 = this->TimeStepValuesRange[0];
    _arg2 = this->TimeStepValuesRange[1];
    vtkDebugMacro(<< this->GetClassName() << " (" << this << "): returning TimeStepRange = ("
                  << _arg1 << "," << _arg2 << ")"); 
  };
  
  virtual void GetTimeStepRange (double _arg[2])
  {
    this->GetTimeStepRange (_arg[0], _arg[1]);
  } 

  // Description:
  // Type of complex result treating
  void SetComplexReal(int flag){
    this->ComplexModeReal = flag;
    // In addition trigger resetting the data value arrays 
    this->resetDataArrays = true;  
    // update pipeline
    this->Modified();
  }
  vtkGetMacro(ComplexModeReal, int);

  void SetComplexImag(int flag){
    this->ComplexModeImag = flag;
    // In addition trigger resetting the data value arrays 
    this->resetDataArrays = true;  
    // update pipeline
    this->Modified();
  }
  vtkGetMacro(ComplexModeImag, int);
  
  void SetComplexAmpl(int flag){
    this->ComplexModeAmpl = flag;
    // In addition trigger resetting the data value arrays 
    this->resetDataArrays = true;  
    // update pipeline
    this->Modified();
  }
  vtkGetMacro(ComplexModeAmpl, int);

  void SetComplexPhase(int flag){
    this->ComplexModePhase = flag;
    // In addition trigger resetting the data value arrays 
    this->resetDataArrays = true;  
    // update pipeline
    this->Modified();
  }
  vtkGetMacro(ComplexModePhase, int);
  
  //! switch if missing results should get filled with 0
  //! 0 = omit empty regions (only partial results available)
  //! 1 = fill empty missing results with 0-valued vector
  void SetFillMissingResults(int);
  vtkGetMacro(FillMissingResults, int);
  vtkBooleanMacro(FillMissingResults, int)

  // Description:
  // Return spatial dimension of grid
  int GetGridDimension(void);
  
  // Description:
  // Return the grid order 
  int GetGridOrder(void);
  
  // Description:
  // Get the number of cell arrays available in the input.
  int GetNumberOfRegionArrays(void);

  // Description:
  // Get/Set whether the cell array with the given name is to
  // be read.
  int GetRegionArrayStatus(const char *name);
  void SetRegionArrayStatus(const char *name, int status);

  // Description:
  // Get the name of the  cell array with the given index in
  // the input.
  const char *GetRegionArrayName(int index);

  // Description:
  // Get the number of named node arrays available in the input.
  int GetNumberOfNamedNodeArrays(void);

  // Description:
  // Get/Set whether the named node array with the given name is to
  // be read.
  int GetNamedNodeArrayStatus(const char *name);
  void SetNamedNodeArrayStatus(const char *name, int status);

  // Description:
  // Get the name of the  named node array with the given index in
  // the input.
  const char *GetNamedNodeArrayName(int index);

  // Description:
  // Get the number of named element arrays available in the input.
  int GetNumberOfNamedElemArrays(void);

  // Description:
  // Get/Set whether the named elem array with the given name is to
  // be read.
  int GetNamedElemArrayStatus(const char *name);
  void SetNamedElemArrayStatus(const char *name, int status);

  // Description:
  // Get the name of the  named elem array with the given index in
  // the input.
  const char *GetNamedElemArrayName(int index);
  
  // Description:
  // Get the coordinates of all nodes in the grid
  void GetNodeCoords(vtkDoubleArray *coords);
  
  
  // ===============================================================
  // CFS History Reader Section
  // ===============================================================
  
  // Description:
  // Name of the history result 
  static vtkInformationStringKey*        RESULT_NAME();
  
  // Description:
  // Vector of Dof names 
  static vtkInformationStringVectorKey*  DOF_NAMES();
  
  // Description:
  // Determines on which entity the result is defined
  //  1: Node
  //  2: Edge (unused)
  //  3: Face (unused)
  //  4: Element
  //  5: Surface element
  //  6: PFEM (unused)
  //  7: Region
  //  8: Surface region
  //  9: Node group
  // 10: Coil
  // 11: Free/Unknown
  static vtkInformationIntegerKey*       DEFINED_ON();
  
  // Description:
  // Name of the entity on which the history result is defined 
  static vtkInformationStringKey*        ENTITY_NAME();
  
  // Description:
  // Numerical type of the history result
  //  0: Unknown (should not occur)
  //  1: Scalar
  //  3: Vector
  //  6: Tensor
  // 32: String (currently unused)
  static vtkInformationIntegerKey*       ENTRY_TYPE();
  
  // Description:
  // Vector of the step numbers at which the history result was saved 
  static vtkInformationIntegerVectorKey* STEP_NUMS();
  
  // Description:
  // Vector of the step values at which the history result was saved 
  static vtkInformationDoubleVectorKey*  STEP_VALUES();
  
  // Description:
  // Unit of the result quantity 
  static vtkInformationStringKey*        UNIT();
  
  // Description:
  // Vector of the entity IDs (strings) on which the history result is defined 
  static vtkInformationStringVectorKey*  ENTITY_IDS();
  
  // Description:
  // Index of the multisequence step 
  static vtkInformationIntegerKey*       MULTI_SEQ_INDEX();

  // Description:
  // Type of the analysis performed in the multisequence step
  // 1: static
  // 2: transient
  // 3: harmonic
  // 4: eigenfrequency
  static vtkInformationIntegerKey*       ANALYSIS_TYPE();
  
  // Description:
  // Get the analysis type of a multisequence step
  vtkInformationVector* GetMultiSequenceInfo(bool history = false);
  
  // Description:
  // Return information about the mesh or history results
  vtkInformationVector* GetResultInfo(unsigned int msStep, bool history = false);
  
  // Description:
  // Get entity IDs of a given history result
  vtkInformation* GetEntityIds(vtkInformation *result);
  
  // Description:
  // Read a history result from a given entity
  void GetHistoryResult(unsigned int msStep,
                        vtkInformation *histResult,
                        char *entityId,
                        vtkDoubleArray *realPart,
                        vtkDoubleArray *imagPart);
                    
  
protected:
  vtkCFSReader();
  ~vtkCFSReader();
  int RequestInformation(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  char *FileName;

private:
  
  // Functions or variables, which should not be Python-wrapped go between BTX and ETX
  //BTX
  
  //! hdf5 reader
  H5CFS::Hdf5Reader in_;
  
  //! map cfs/hdf5 element type to vtk native ones
  VTKCellType GetCellIdType( H5CFS::ElemType type);
  
  
  //! save vector into vtkDoubleArray
  vtkDoubleArray* SaveToArray( const std::vector<double>& vals,
                               const std::vector<std::string>& dofNames,
                               unsigned int numEntities,
                               const std::string& name );

  //! Pretty print frequency including suffix (Hz, kHz, MHz...)
  void PrettyPrintFreq(double freq, std::string& str);
  
  //! Pretty print frequency including suffix (ns, ms, s)
  void PrettyPrintTime(double time, std::string& str);
		       
  //! dimension of the grid 
  int dimension_;
  
  //! order of grid
  int gridOrder_;
  
  //! vector containing all region names
  std::vector<std::string> regionNames_;

  //! vector containing all named nodes
  std::vector<std::string> namedElemNames_;

  //! vector containing all named elems
  std::vector<std::string> namedNodeNames_;

  //! map (region, globalNodeNumber)->(regionLocalNodeNumber)
  std::vector<std::vector< unsigned int> > nodeMap_;
  
  //! time steps of current multisequence step
  std::vector<double> stepVals;
  
  //! vector with step values having a result
  std::vector<unsigned int> stepNums; 
  
  //! multiblock dataset, which contains for each
  //! region an initialized grid
  vtkMultiBlockDataSet * mbDataSet;

  //! reduced multiblock dataset, which contains
  //! only blocks for currently active regions
  vtkMultiBlockDataSet * mbActiveDataSet;
  
  //! flag array indicating active regions
  std::map<std::string, int> regionSwitch;

  //! flag array indicating named nodes to read
  std::map<std::string, int> namedNodeSwitch;

  //! flag array indicating named elems to read
  std::map<std::string, int> namedElemSwitch;
  
  //! flag indicating change of multisequence step
  bool msStepChanged;
  
  //! Analysis type of multi-sequence steps with mesh results
  std::map<unsigned int, H5CFS::AnalysisType> resAnalysisType_;
  
  //! ResultInfo of mesh results per multi-sequence step
  std::map<unsigned int, std::vector< shared_ptr<H5CFS::ResultInfo> > >
      meshResultInfos_;

  //! Analysis type of multi-sequence step with history results
  std::map<unsigned int, H5CFS::AnalysisType> histAnalysisType_;
  
  //! ResultInfo of history results per multi-sequence step
  std::map<unsigned int, std::vector< shared_ptr<H5CFS::ResultInfo> > >
      histResultInfos_;

  //! global steps per multisequence where ANY result is available
  std::map<unsigned int, std::set< std::pair<unsigned int, double> > >
      globalResultSteps_;
  
  //ETX
  
  //! current multisequence step
  unsigned int MultiSequenceStep;
  
  //! analysis type of current multisequence step
  H5CFS::AnalysisType analysisType;
  
  //! current time step (for discrete time steps)
  unsigned int TimeStep;
  
  //! current time step / frequency value pretty printed
  double timeFreqVal;

  //! pretty formatted time value / frequency value
  std::string timeFreqValStr;
  
  //! The time value which the pipeline requests
  double requestedTimeValue;
  
  //! complex mode
  unsigned int ComplexModeReal;
  unsigned int ComplexModeImag;
  unsigned int ComplexModeAmpl;
  unsigned int ComplexModePhase;

  //! switch if missing results should get filled with 0
  //! 0 = omit empty regions (only partial results available)
  //! 1 = fill empty missing results with 0-valued vector
  unsigned int FillMissingResults; 
  
  // add dimensions to array names
  int AddDimensionsToArrayNames;

  // Interpret harmonic data as transient mode shape
  int HarmonicDataAsModeShape;
  
  // Are we dealing with harmonic data?
  bool harmonicData;
  
  //! number of time/freq steps in current ms step
  unsigned int NumberOfTimeSteps;
  
  //! array with first/last time/freq step value
  int TimeStepNumRange[2];

  //! array with first/last time/freq step value
  double TimeStepValuesRange[2];
  
  //! array with first/last ms step number
  int MultiSequenceRange[2];
  
  //! flag if hdf5 file is already read in
  bool isInitialized;

  //! flag if hdf5 infos (region names, named node/elems) have been read in
  bool hdf5InfoRead;
  
  //! flag, if region settings were modified
  bool activeRegionsChanged;

  //! flag, to reset the data value arrays (e.g. after
  //! switching the complex mode etc.)
  bool resetDataArrays;

  
  //! Actually open the file and reads region and group names
  void ReadHdf5Info();
  
  void ReadFile(vtkMultiBlockDataSet *output);
  void ReadNodes(vtkMultiBlockDataSet *output);
  void ReadCells(vtkMultiBlockDataSet *output);
  
  //! Create single grid from elements of one region / group
  void AddElements( vtkUnstructuredGrid *actGrid,
                    unsigned int blockIndex,
                    const std::vector<unsigned int>& elems,
                    std::vector<H5CFS::ElemType>& types,
                    std::vector<std::vector<unsigned int> >& connect );

  void ReadNodeCellData(vtkMultiBlockDataSet *output, bool isNode);
  
  //! Update multiblock dataset according to current region/block settings
  void UpdateActiveRegions(); 

  vtkCFSReader(const vtkCFSReader&);  // Not implemented.
  void operator=(const vtkCFSReader&);  // Not implemented.
};

#endif
