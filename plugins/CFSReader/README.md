openCFS Paraview Plugin
=======================

The plugin allows to read `*.cfs` files.

Build instructions
------------------

* ensure you have the prerequisites: linux+docker
* use the [paraview plugin builder](https://gitlab.kitware.com/ftoth/paraview-plugin-builder) to
* build paraview (in the correct version), then 
* build the plugin.n dir with the top level `CMakeLists.txt` with `project(CFSReader)`.
* the plugin is collected in the `output` directory

Use the following steps or check the pipeline
```
cd <plugin-builder> # change to the dir of the plugin builder
./run_build_paraview v5.8.1
./run_build_plugin.sh -d <plugin-dir> v5.8.1 # <plugin-dir> must contain the topl level CMakeLists of the PLUGIN
```



Installation
------------

The plugin can be installed in [official ParaView downloads](https://www.paraview.org/download/):
1. donwload and extract
2. create the directory `<install>/lib/paraview-<version>/plugins/CFSReader/` and put in `CFSReader.so`
2. edit the file `<install>/lib/paraview-<version>/plugins/paraview.plugins.xml` to add an enrty for `<Plugin name="CFSReader" auto_load="1" />` to enable auto-loading the plugin

Tipps for debugging
-------------------

* the `run_build_plugin.sh <tag>` creates a docker image called `paraview:<tag>`, it will be re-uesd for the plugin build
* if the dependency installation in the docker container fails, delete it and restart
* if networking does not work in the container, restart your docker daemon 

Resources
---------

* [plugin builder](https://gitlab.kitware.com/paraview/paraview-plugin-builder/)
* [paraview plugin howto](https://kitware.github.io/paraview-docs/latest/cxx/PluginHowto.html)
* [Elevation Filter Example](https://gitlab.kitware.com/paraview/paraview/-/tree/master/Examples/Plugins/ElevationFilter)
