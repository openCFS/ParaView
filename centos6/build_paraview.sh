#!/bin/bash

function exit_fail
{
  echo "ERROR: $1" 1>&2
  exit 1
}

PV_BIN_DIR=/home/vagrant/paraview_build

mkdir $PV_BIN_DIR || exit_fail "cannot create binary directory '${PV_BIN_DIR}'"
cd $PV_BIN_DIR

# use software collection environment to configure and build
# run something in environment: ´scl <collection1> [<collection2> ...] -- <command>´
# see ´man scl´ for details or check [online](https://www.softwarecollections.org)
SCL='scl enable devtoolset-3'
$SCL -- cmake /home/vagrant/paraview_source || exit_fail 'configure failed'
$SCL -- make || exit_fail 'make failed'

# pack paraview
cd build
$SCL -- ctest -R cpack-paraview || exit_fail 'pack'
archive=$(pwd)/$(ls ParaView-*.tar.gz)
echo $archive
# copy out packed paraview
cd /home/vagrant/paraview_source/centos6
# full paraview archive
cp $archive . 
# collect the plugin files
/home/vagrant/paraview_source/collect_plugin.sh $archive

exit 0
