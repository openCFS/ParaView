Official ParaView Build System
==============================

The official ParaView available [online](http://www.paraview.org/download/) is built with the options given on the [official wiki](http://www.paraview.org/Wiki/ParaView/Binaries#Linux-x64).
According to the [ParaView developer mailing list](http://public.kitware.com/pipermail/paraview-developers/2017-January/subject.html) the build system for ParaView 5.2 is centos 6 with devtoolset-3. 
The `Vagrantfile` provides the official centos 6 box, and the script `bootstrap.sh` sets up the build environment and installs build dependencies.
Additionally, the build can be done by the CI/CD system of gitlab.

Gitlab Runners
-------------------
We host a gitlab-runner with the tag `paraview` which is able to build on the official build system.
The build process is detailled in [.gitlab-ci.yml](.gitlab-ci.yml), which defines the following things
* pipelines run for branches called `master` and `test-*` 
* the build directory on the runner is never cleared, except when a pipeline is run via a `schedule`. This must be done in order to _switch to a higher PV version_.
* after the build, we pack the plugin via [collect_plugin.sh](collect_plugin.sh) and provide it as build artifacs for download.
* the final plugin (on the master branch) is copied to draco (but not installed!)

Using the Box
-------------------
Using the box requires [vagrant](https://www.vagrantup.com/) and one supported provider (virtualiser).
Everything was tested using [virtualbox](https://www.virtualbox.org/) as provider.
The source for the metabuild is currently handed to the box via a *shared folder*.
Therefore, one needs to install the *virtualbox guest additions* to the box, which can be done automatically by installing the vagrant plugin [vbguest](https://github.com/dotless-de/vagrant-vbguest) via `vagrant plugin install vagrant-vbguest`.

To start the box use in this directory, then you can connect to the box via ssh
```
vagrant up
vagrant ssh
```
The first time you run the box on the host machine the bootstrap script will run.
You can stop the box, or destroy (remove it from virtualbox) it by
```
vagrant halt
vagrant destroy
```

Building in the Box
-------------------
Building with the box is done with the script `build_paraview.sh` which

 1. creates a build dir
 2. configures metabuild
 3. builds
 4. packs and copies archive to shared folder

Simply open a shell on the box (`vagrant ssh`) and execute the script
```
nohup /home/vagrant/paraview_source/centos6/build_paraview.sh > build.log &
```
You may logout from the box and wait for the build to finish.
The created binary archive should appear in `<metabild source>/centos6/` then everything is done.
