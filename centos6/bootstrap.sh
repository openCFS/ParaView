#!/bin/bash

function exit_fail
{
  echo "ERROR: $1" 1>&2
  exit 1
}
# install dependencies for superbuild
sudo yum -y install git

# install devtoolset-3
sudo yum install -y centos-release-scl || exit_fail 'repo install'
sudo yum-config-manager --enable rhel-server-rhscl-6-rpms || exit_fail 'repo enable'
sudo cp "/vagrant/rhscl-devtoolset-3.repo" /etc/yum.repos.d/. || exit_fail 'repo copy'
sudo yum -y install devtoolset-3 || exit_fail 'devtoolset install'

# install ParaView build depenancies
sudo yum -y install libX11-devel libXext-devel libXtst-devel 
# install VTK dependencies
sudo yum -y install mesa-libGL-devel libXt-devel

# download & install cmake
echo "downloading cmake, this may take a while ..."
wget -q --no-check-certificate https://cmake.org/files/v3.7/cmake-3.7.2-Linux-x86_64.tar.gz || exit_fail 'download of cmake failed'
sudo tar -zxvf cmake-3.7.2-Linux-x86_64.tar.gz -C /usr/local --strip-components=1 || exit_fail 'installation of cmake failed'

exit 0
