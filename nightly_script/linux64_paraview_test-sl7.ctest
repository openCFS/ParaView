# run this with ctest [-VV] -S linux64_paraview_test-sl7.ctest

# before running the script you need to checkout the source using git-svn
# git svn clone https://cfs.mdmt.tuwien.ac.at/svn/CFS++_ParaView --stdlayout cfs_paraview

SET(CTEST_SOURCE_DIRECTORY "$ENV{HOME}/paraview/cfs_paraview")
SET(CTEST_BINARY_DIRECTORY "$ENV{HOME}/paraview/cfs_paraview_build")
SET(CTEST_CMAKE_GENERATOR "Unix Makefiles")
SET(CTEST_PROJECT_NAME "Paraview metabuild")

# put /sbin/ldconfig in PATH (http://public.kitware.com/pipermail/paraview/2017-January/thread.html#39046)
set(ENV{PATH} "$ENV{PATH}:/sbin")

# set display
set(ENV{DISPLAY} ":0")

set(CTEST_SITE "test-sl7")
set(CTEST_BUILD_NAME "Paraview 5.3 metabuild")

# somehow CTEST_START_WITH_EMPTY_BINARY_DIRECTORY does not work?! So do it also manually
#file(REMOVE_RECURSE "${CTEST_BINARY_DIRECTORY}")
#SET(CTEST_START_WITH_EMPTY_BINARY_DIRECTORY TRUE)

SET(BUILDTYPE "RELEASE")

message("Start dashboard...")
# experimental makes separates PV from the cfs buils
ctest_start(Experimental)

message("  Update")
find_program(CTEST_GIT_COMMAND NAMES git)
set(CTEST_UPDATE_TYPE "git") # use git
set(CTEST_UPDATE_COMMAND "${CTEST_GIT_COMMAND}")
set(CTEST_GIT_UPDATE_CUSTOM "${CTEST_GIT_COMMAND};svn;rebase") # use git-svn
# It is essential to set the build command, otherwise qt fails to build, probably due to race conditions
# This is only an issue using calling metabuild via ctest, strange indeed :(
set(CTEST_BUILD_COMMAND "/usr/bin/make -j2")
ctest_update(SOURCE "${CTEST_SOURCE_DIRECTORY}" RETURN_VALUE res)

message("  Configure")
set(CTEST_BUILD_COMMAND "/usr/bin/make")
SET(CTEST_INITIAL_CACHE
   "CMAKE_BUILD_TYPE:STRING=Release")

FILE(WRITE "${CTEST_BINARY_DIRECTORY}/CMakeCache.txt" ${CTEST_INITIAL_CACHE})

ctest_configure(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE res)

message("  Build")
ctest_build(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE res)

message("  Test")
ctest_test(BUILD "${CTEST_BINARY_DIRECTORY}/build" RETURN_VALUE res)


message("  Submit")
ctest_submit(RETURN_VALUE res)

message("  All done")

