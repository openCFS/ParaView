## This file should be placed in the root directory of your project.
## Then modify the CMakeLists.txt file in the root directory of your
## project to incorporate the testing dashboard.
## # The following are required to uses Dart and the Cdash dashboard
##   ENABLE_TESTING()
##   INCLUDE(CTest)
SET(CTEST_PROJECT_NAME "Paraview Metabuild")
SET(CTEST_NIGHTLY_START_TIME "00:00:00 CET")

SET(CTEST_DROP_METHOD "http")
SET(CTEST_DROP_SITE "cfs.mdmt.tuwien.ac.at:1022")
SET(CTEST_DROP_LOCATION "/cdash/submit.php?project=CFS")
SET(CTEST_DROP_SITE_CDASH TRUE)
